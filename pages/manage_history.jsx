import React from 'react'
import { useState, Fragment, useEffect } from 'react'
import { Dialog, Transition, Listbox, Switch } from '@headlessui/react'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import { useRouter } from 'next/router'

import Flatpickr from "react-flatpickr";
import "flatpickr/dist/themes/material_blue.css";

import {
  ChevronRightIcon
} from '@heroicons/react/solid'
import ReactMde from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";
import dynamic from 'next/dynamic';
import '@uiw/react-markdown-preview/markdown.css';
const MarkdownPreview = dynamic(
  () => import("@uiw/react-markdown-preview").then((mod) => mod.default),
  { ssr: false }
);

function loadSuggestions(text) {
  return new Promise((accept, reject) => {
    setTimeout(() => {
      const suggestions = [
        {
          preview: "Andre",
          value: "@andre"
        },
        {
          preview: "Angela",
          value: "@angela"
        },
        {
          preview: "David",
          value: "@david"
        },
        {
          preview: "Louise",
          value: "@louise"
        }
      ].filter(i => i.preview.toLowerCase().includes(text.toLowerCase()));
      accept(suggestions);
    }, 250);
  });
}


const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});

const mock_dataCard = {
  title: "CLOSE",
  items: {
    name: "Report Summary",
    create_time: "09/06/2021 16:42:07",
    start_time: "10/06/2021 19:12:07",
    done_time: "10/06/2021 20:42:07",
    role: {
      name: "BD",
    },
    assign: {
      "id": 4,
      "name": "Wade Cooper",
      "avatar": "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
    },
    estimate: {
      hour: 1,
      minute: 30,
    },
    spent: {
      hour: 1,
      minute: 30,
    },
    "comment": [
      {
        "id": "b25bbd39-cee4-40b5-85a1-ad87b41e02ad",
        "name": "Jame",
        "description": "เปลี่ยนช่องทางการจัดเก็บเอกสารไว้ที่ลิ้งนี้ https://drive.google.com/drive/u/0/my-drive",
        "image": "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
      },
      {
        "id": "396e49fd-78d3-465c-b3a1-b5106fcddb2f",
        "name": "Beam",
        "description": "ไฟล์แก้ไขแล้ว",
        "image": "https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
      }
    ]
  }
}


const manage_history = () => {
  const [editStartTime, setEditStartTime] = useState(false)
  const [newStartTime, setNewStartTime] = useState('')
  const [editDoneTime, setEditDoneTime] = useState(false)
  const [newDoneTime, setNewDoneTime] = useState('')
  const [editSpent, setEditSpent] = useState(false)
  const [newSpent, setNewSpent] = useState({ hour: '0', minute: '0' })
  const [newCommentsBoard, setNewCommentsBoard] = useState()

  const router = useRouter()
  const timesheet_id = router.query.id

  const [dataCard, setDataCard] = useState()
  const [project, setProject] = useState()
  const [task, setTask] = useState()
  const [subTask, setSubtask] = useState()

  const [statusEditComments, setstatusEditComments] = useState({ status: false, index: '' })
  const [newEditComments, setnewEditComments] = useState('')
  const [selectedTabEditComments, setSelectedTabEditComments] = useState("write");

  const submitEdit = async (value) => {
    if (newStartTime || newDoneTime || newSpent) {
      const { id, startTime, stopTime, spentTime } = dataCard
      const newdata = {
        "id": `${id}`,
        "startTime": value.id === 'start_date' ? newStartTime : startTime,
        "stopTime": value.id === 'done_date' ? newDoneTime : stopTime,
        "spentTime": value.id === 'spent' ? (newSpent.hour * 60 + parseInt(newSpent.minute)) : spentTime ? spentTime : 0,
      }
      console.log(newdata)

      try {
        const response = await fetch(`/api/timesheet/edit`, {
          method: 'PATCH',
          body: JSON.stringify(newdata),
        })
      } catch (error) {
        console.log(error);
      }
      loadTimeSheetById(timesheet_id)

      if (value.id === 'start_date') {
        setNewStartTime('')
        setEditStartTime(false)
      } else if (value.id === 'done_date') {
        setNewDoneTime('')
        setEditDoneTime(false)
      } else if (value.id === 'spent') {
        setNewSpent({ hour: '', minute: '' })
        setEditSpent(false)
      }
    } else {
      if (value.id === 'start_date') {
        setNewStartTime('')
        setEditStartTime(false)
      } else if (value.id === 'done_date') {
        setNewDoneTime('')
        setEditDoneTime(false)
      } else if (value.id === 'spent') {
        setNewSpent({ hour: '', minute: '' })
        setEditSpent(false)
      }
    }
  }

  const submitEditComment = async (idComment) => {
    const { id, items } = dataCard
    if (newEditComments) {
      const newValue = {
        idCard: id,
        idSubtask: items.id,
        newValue: newEditComments,
        idComment: idComment
      }
      console.log(newValue)
      setnewEditComments('')
      setstatusEditComments({ status: false, index: '' })
    }
    setnewEditComments('')
    setstatusEditComments({ status: false, index: '' })
  }

  const loadTimeSheetById = async (_timeSheetID) => {
    console.log("load")
    const url = `/api/timesheet/gettimesheetbyid/?timesheet_id=${_timeSheetID}`
    try {
      const result = await fetch(url, { method: 'get' })
      const data = await result.json()
      setDataCard(data)
      setProject(data.project)
      setTask(data.task)
      setSubtask(data.subtask)
    } catch (error) {
      setDataCard(mock_dataCard)
      console.log(error);
    }
  }

  const isoToUtc = (isoString) => {
    var utcDate = new Date(isoString).toLocaleDateString('en-GB')
    var utcTime = new Date(isoString).toLocaleTimeString('en-GB')
    return [utcDate, utcTime]
  }

  const handleKeyPressTimeSpent = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  }

  useEffect(() => {
    loadTimeSheetById(timesheet_id)
  }, [])

  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
      <nav className="mx-5 w-full">
        <ol className="list-reset flex items-center space-x-1">
          <li><a href="history" className=" text-gray-400">Manage history</a></li>
          <li><ChevronRightIcon className='w-6 h-6 mt-1 text-gray-400' /> </li>
          <li>History - Jame Ji</li>
        </ol>
      </nav>
      <div className="bg-white rounded-md shadow-md mt-5 py-1 w-full h-full divide-y divide-gray-200">
        <div className=' py-1 space-y-4 sm:py-0 sm:space-y-0 sm:px-10'>
          <div
            className={`${dataCard && dataCard.status === 'DOING' ? 'bg-ts-green-400'
              : dataCard && dataCard.status === 'DONE' ? 'bg-ts-blue-500'
                : dataCard && dataCard.status === 'CLOSE' ? 'bg-ts-red-600'
                  : 'bg-ts-yellow-500'} text-white w-max px-4 py-1 mt-6 rounded-md`}
          >
            {dataCard && dataCard.status}
          </div>

          {/* section 1 */}
          <div className='space-y-1 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-3'>
            {/* Create at */}
            <div className='sm:col-span-1'>
              <label
                htmlFor='group-name'
                className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
              >
                Create at
              </label>
            </div>
            <div className='sm:col-span-1 flex items-center '>
              <p>
                {dataCard && isoToUtc(dataCard.createdDate)[0]} {dataCard && isoToUtc(dataCard.createdDate)[1]}
              </p>
            </div>

            {/* Start at */}
            <div className='sm:col-span-2 sm:grid sm:grid-cols-3'>
              <div className='flex sm:col-span-1'>
                <label
                  htmlFor='group-name'
                  className='flex text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold ml-20'
                >
                  Start at
                </label>
              </div>

              <div className='flex sm:col-span-2 items-center'>
                {editStartTime ?
                  <div className="flex justify-between w-full">
                    <div className='pr-2 pb-3'>
                      <Flatpickr
                        className='flex border border-ts-gray-500 rounded-md w-full mr-3'
                        placeholder='Select Start Date'
                        options={{
                          enableTime: true,
                          dateFormat: "d/m/Y H:i",
                        }}
                        onChange={([date]) => {
                          setNewStartTime(date.toISOString());
                        }}
                      />
                    </div>
                    <div className="flex">
                      <button type="button" className="m-1" onClick={() => setEditStartTime(false)}>
                        <p className="underline text-ts-gray-600 text-base">
                          Cancel
                        </p>
                      </button>
                      <button type="button" className="m-1" onClick={() => submitEdit({ id: 'start_date' })}>
                        <p className="underline text-ts-gray-600 text-base">
                          save
                        </p>
                      </button>
                    </div>
                  </div>
                  :
                  <div className="flex justify-between w-full">
                    <p>{dataCard && <span>{isoToUtc(dataCard.startTime)[0]} {isoToUtc(dataCard.startTime)[1]}</span>}</p>
                    <button type="button" onClick={() => setEditStartTime(true)}>
                      <p className="underline text-ts-gray-600 text-base mr-12">
                        Edit
                      </p>
                    </button>
                  </div>}

              </div>
            </div>
          </div>

          <div className='space-y-1 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-3'>
            {/* Project */}
            <div>
              <label
                htmlFor='group-name'
                className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
              >
                Project
              </label>
            </div>
            <div className='sm:col-span-1 flex items-center '>
              <p>
                {project && project.name}
              </p>
            </div>

            {/* Done at */}
            <div className='sm:col-span-2 sm:grid sm:grid-cols-3'>
              <div className='sm:col-span-1'>
                <label
                  htmlFor='group-name'
                  className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold ml-20'
                >
                  Done at
                </label>
              </div>

              <div className='flex sm:col-span-2 items-center'>
                {editDoneTime ?
                  <div className="flex justify-between w-full">
                    <Flatpickr
                      className='flex border border-ts-gray-500 rounded-md w-full mr-3'
                      placeholder='Select Done Date'
                      options={{
                        enableTime: true,
                        dateFormat: "d/m/Y H:i",
                      }}
                      onChange={([date]) => {
                        setNewDoneTime(date.toISOString());
                      }}
                    />
                    <div className="flex">
                      <button type="button" className="m-1" onClick={() => setEditDoneTime(false)}>
                        <p className="underline text-ts-gray-600 text-base">
                          Cancel
                        </p>
                      </button>
                      <button type="button" className="m-1" onClick={() => submitEdit({ id: 'done_date' })}>
                        <p className="underline text-ts-gray-600 text-base">
                          save
                        </p>
                      </button>
                    </div>
                  </div>
                  :
                  <div className="flex justify-between w-full">
                    <p>{dataCard && <span>{isoToUtc(dataCard.stopTime)[0]} {isoToUtc(dataCard.stopTime)[1]}</span>}</p>
                    <button type="button" onClick={() => setEditDoneTime(true)}>
                      <p className="underline text-ts-gray-600 text-base mr-12">
                        Edit
                      </p>
                    </button>
                  </div>}

              </div>
            </div>
          </div>

          <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-3'>
            <div>
              <label
                htmlFor='group-name'
                className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
              >
                Task
              </label>
            </div>
            <div className='sm:col-span-2 flex items-center'>
              <p>
                {task && task.name}
              </p>
            </div>
          </div>
        </div>

        <div className='py-1 space-y-3 sm:py-0 sm:space-y-0 sm:px-10'>
          {dataCard &&
            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-3 sm:px-6 sm:py-3 flex items-center'>
              <div>
                <label
                  htmlFor='group-name'
                  className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
                >
                  Subtask name
                </label>
              </div>
              <div className='sm:col-span-3 flex justify-between pr-12'>
                <div className="flex justify-between w-full">
                  <p>{subTask && subTask.name}</p>
                </div>
              </div>
            </div>
          }
          {/* Description */}
          {dataCard &&
            <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-3 sm:px-6 sm:py-3'>
              <div>
                <label
                  htmlFor='group-description'
                  className='text-sm font-medium text-gray-900 sm:mt-px font-bold'
                >
                  Description
                </label>
              </div>
              <div className='sm:col-span-3 flex justify-between pr-12'>
                <p>{subTask && subTask.description}</p>
              </div>
            </div>}
        </div>
        <div className='py-1 space-y-3 sm:py-0 sm:space-y-0 sm:px-10'>

          {/* Role */}
          {dataCard &&
            <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-5'>
              <div>
                <label
                  htmlFor='group-name'
                  className=' text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
                >
                  Role
                </label>
              </div>
              <div className='sm:col-span-3 flex justify-between pr-12'>
                <p>{subTask && subTask.role.name}</p>
              </div>
            </div>}

          {/* assignee */}
          {dataCard &&
            <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-5'>
              <div>
                <label
                  htmlFor='group-name'
                  className=' text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
                >
                  Assignee
                </label>
              </div>
              <div className='sm:col-span-3 flex justify-between pr-12'>
                <p>{subTask && subTask.profile.name}</p>
              </div>

            </div>
          }
          {/* estimated time */}
          {dataCard &&
            <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-5'>
              <div>
                <label
                  htmlFor='group-name'
                  className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
                >
                  Estimated time
                </label>
              </div>
              <div className='sm:col-span-3 flex justify-between pr-12'>
                <div className="flex">
                  <p className="text-base">
                    {dataCard && parseInt(dataCard.estimatedTime / 60)}
                  </p>
                  <p className="mx-2 text-ts-gray-600">
                    hour(s)
                  </p>
                  <p className="text-base ml-4 ">
                    {dataCard && dataCard.estimatedTime % 60}
                  </p>
                  <p className=" mx-2 text-ts-gray-600">
                    minute(s)
                  </p>
                </div>
              </div>
            </div>
          }
          {/* spent time */}
          {dataCard &&
            <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-4 sm:gap-4 sm:px-6 sm:py-5'>
              <div>
                <label
                  htmlFor='group-name'
                  className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 font-bold'
                >
                  Spent time
                </label>
              </div>
              {editSpent ?
                <div className='sm:col-span-3 flex justify-between pr-12'>
                  <div className='sm:col-span-2 flex items-center'>
                    <input
                      type="number"
                      onKeyPress={(event) => handleKeyPressTimeSpent(event)}
                      min="0"
                      value={newSpent.hour} onChange={(e) => setNewSpent({ hour: e.target.value, minute: newSpent.minute })}
                      className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                    />

                    <p className="mx-1">
                      hour(s)
                    </p>
                    <input
                      type="number"
                      onKeyPress={(event) => handleKeyPressTimeSpent(event)}
                      min="0"
                      value={newSpent.minute} onChange={(e) => setNewSpent({ hour: newSpent.hour, minute: e.target.value })}
                      className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                    />
                    <p className=" mx-1">
                      minute(s)
                    </p>
                  </div>
                  <div className="flex ml-3">
                    <button type="button" className="m-1" onClick={() => setEditSpent(false)}>
                      <p className="underline text-ts-gray-600 text-base">
                        Cancel
                      </p>
                    </button>
                    <button type="button" className="m-1" onClick={() => submitEdit({ id: 'spent' })}>
                      <p className="underline text-ts-gray-600 text-base">
                        save
                      </p>
                    </button>
                  </div>
                </div>
                :
                <div className='sm:col-span-3 flex justify-between pr-12'>
                  <div className="flex">
                    <p className="text-base">
                      {dataCard && parseInt(dataCard.spentTime / 60)}
                    </p>

                    <p className="mx-2 text-ts-gray-600">
                      hour(s)
                    </p>
                    <p className="text-base ml-4 ">
                      {dataCard && dataCard.spentTime % 60}
                    </p>
                    <p className=" mx-2 text-ts-gray-600">
                      minute(s)
                    </p>
                  </div>
                  <button type="button" onClick={() => setEditSpent(true)}>
                    <p className="underline text-ts-gray-600 text-base">
                      Edit
                    </p>
                  </button>
                </div>
              }
            </div>
          }
        </div>

        <div className='py-2 space-y-4 sm:py-0 sm:space-y-1 sm:px-10'>
          <button className='p-1 my-2 font-semibold rounded-lg border-2 border-gray-500 ml-5'>Add label</button>
        </div>
        {/* Activity */}
        <div className='py-2 space-y-4 sm:py-0 sm:space-y-1 sm:px-10'>
          {dataCard &&
            <div className="flex flex-col items-center px-4 py-5">
              {mock_dataCard.items.comment && mock_dataCard.items.comment.length > 0 &&
                <div className="w-full px-0 mb-8">
                  <p className="font-bold text-base ">
                    Activity
                  </p>
                  <div className="mt-5 ml-5">
                    {newCommentsBoard ? newCommentsBoard.map((element, index) => {
                      return (
                        <div key={index} className=" flex  w-full py-2 px-3 bg-ts-white-100 border border-ts-gray-500 rounded-md my-2 ">
                          <div className="px-2">
                            <img className="w-6 h-6 rounded-full" src={element.image} alt={element.image} />
                          </div>
                          <div className="ml-3 w-full">
                            <div className="flex justify-between w-full">
                              <p>{element.name}</p>
                              {!statusEditComments.status && !(statusEditComments.index === index) && <button type="button" onClick={() => (setstatusEditComments({ status: true, index: index }), setnewEditComments(''))}>
                                <img src="edit-icon.svg" alt="edit-icon" />
                              </button>}
                            </div>
                            {statusEditComments.status && (statusEditComments.index === index)
                              ? <div className="px-5 py-7">
                                <ReactMde
                                  value={newEditComments ? newEditComments : element.description}
                                  onChange={setnewEditComments}
                                  selectedTab={selectedTabEditComments}
                                  onTabChange={setSelectedTabEditComments}
                                  generateMarkdownPreview={markdown =>
                                    Promise.resolve(converter.makeHtml(markdown))
                                  }
                                  loadSuggestions={loadSuggestions}
                                  childProps={{
                                    writeButton: {
                                      tabIndex: -1
                                    }
                                  }}
                                />
                              </div>
                              :
                              <div>
                                <MarkdownPreview source={element.description} />
                              </div>}
                          </div>
                          {statusEditComments.status && (statusEditComments.index === index) &&
                            <div>
                              <div className="flex justify-between">
                                <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => (setstatusEditComments({ status: false, index: '' }), setnewEditComments(''))}>
                                  cancel
                                </button>
                                <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => submitEditComment(index)}>
                                  save
                                </button>
                              </div>
                            </div>
                          }
                        </div>
                      )
                    }) : mock_dataCard.items.comment.map((element, index) => {
                      return (
                        <div key={index} className=" flex  w-full py-2 px-3 bg-ts-white-100 border border-ts-gray-500 rounded-md my-2 ">
                          <div className="px-2">
                            <img className="w-6 h-6 rounded-full" src={element.image} alt={element.image} />
                          </div>
                          <div className="ml-3 w-full">
                            <div className="flex justify-between w-full">
                              <p>{element.name}</p>
                              {!statusEditComments.status && !(statusEditComments.index === index) && <button type="button" onClick={() => (setstatusEditComments({ status: true, index: index }), setnewEditComments(''))}>
                                <img src="edit-icon.svg" alt="edit-icon" />
                              </button>}
                            </div>
                            {statusEditComments.status && (statusEditComments.index === index)
                              ? <div className="px-5 py-7">
                                <ReactMde
                                  value={newEditComments ? newEditComments : element.description}
                                  onChange={setnewEditComments}
                                  selectedTab={selectedTabEditComments}
                                  onTabChange={setSelectedTabEditComments}
                                  generateMarkdownPreview={markdown =>
                                    Promise.resolve(converter.makeHtml(markdown))
                                  }
                                  loadSuggestions={loadSuggestions}
                                  childProps={{
                                    writeButton: {
                                      tabIndex: -1
                                    }
                                  }}
                                />
                              </div>
                              :
                              <div>
                                <MarkdownPreview source={element.description} />
                              </div>}
                          </div>
                          {statusEditComments.status && (statusEditComments.index === index) &&
                            <div>
                              <div className="flex justify-between">
                                <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => (setstatusEditComments({ status: false, index: '' }), setnewEditComments(''))}>
                                  cancel
                                </button>
                                <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => submitEditComment(element.id)}>
                                  save
                                </button>
                              </div>
                            </div>
                          }
                        </div>
                      )
                    })}
                  </div>
                </div>
              }

            </div>
          }
        </div>
      </div>

    </div>
  )
}

export default manage_history
manage_history.Layout = MainLayout
manage_history.current = 'history'
manage_history.title = 'History'
manage_history.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}