import MainLayout from '../components/MainLayout'
import { getSession } from 'next-auth/client'
import { useState,useEffect } from 'react'
import { useRouter } from 'next/router'

const index = () => {
  const router = useRouter()
  const [hello, sethello] = useState("n/a");
  async function callBackend() {
    const res = await fetch('/api/hello');
    const text = await res.text();
    sethello(text);
  }

  const handleRedirect = () => {
    router.push('/timesheet')
  }

  useEffect(() => {
    handleRedirect()
  }, [])

  return (
    <>
    
    </>
  )


}

export default index
index.Layout = MainLayout;
index.current = 'timesheet'
index.title = 'Timesheet'
index.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}
