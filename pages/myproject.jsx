import { useState, useEffect } from 'react'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import HeaderBox from '../components/HeaderBox'
import ButtonSort from '../components/ButtonSort'
import DotsVerticalDropDown from '../components/DotsVerticalDropDown'
import TaskCreateForm from '../components/TaskCreateForm'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useSession } from "next-auth/client"


import {
  DotsVerticalIcon,
  UsersIcon,
  ChevronRightIcon
} from '@heroicons/react/solid'

import {
  FolderAddIcon
} from '@heroicons/react/outline'

import Pagination from '../components/layout/Pagination'

const myproject = () => {
  const [session, loading] = useSession()
  const router = useRouter()
  const project_id = router.query.id
  const [project, setProject] = useState()
  const [showCreateForm, setShowCreateForm] = useState(false)

  const getProjectById = async () => {
    try {
      const response = await fetch(`api/project/viewprojectbyid?project_id=${project_id}`)
      const data = await response.json()
      setProject(data)
    } catch (error) {
      console.log(error);
    }
  }

  const onSaveTask = async (data) => {
    getProjectById()
    setShowCreateForm(false)
  }

  useEffect(() => {
    getProjectById()
  }, [project_id])

   const handleSendIDTask = (id) => {
    router.push({
      pathname: `/subtask`,
      query: {
        id: id,
        project_id:project_id
      }
    })
  }


  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">

      <nav class="mx-5 w-full">
        <ol class="list-reset flex items-center space-x-1">
          <li><a href="#" class=" text-gray-400">My Projects</a></li>
          <li><ChevronRightIcon className='w-6 h-6 mt-1 text-gray-400' /> </li>
          <li>{project && project.name}</li>
        </ol>
      </nav>

      <HeaderBox title={project && project.name} content={project && project.projectMembers.length + ' member(s)'} />
      {session?.user?.admin && <div className='flex justify-end'>
        <div className='flex justify-between col-span-2'>
          <button className='button-default hover:bg-ts-blue-800'
            onClick={() => setShowCreateForm(true)}
          >
            <FolderAddIcon className='w-5 h-5' />
            <p>Add Task</p>
          </button>
        </div>

      </div>}



      <div className="flex flex-col w-full rounded-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 divide-y divide-divider">
        <div className='flex w-full px-8 py-2'>
          <div className='w-4/12 items-center font-bold'>Task Name</div>
          <div className='w-5/12 items-center font-bold justify-center'>Progess</div>
          <div className='w-3/12 items-center font-bold'>Member</div>
        </div>

        <div className='flex flex-col w-full divide-y divide-divider px-8'>
        
          {project && project.tasks.length > 0 ? project.tasks.map((task) => {
            return (
              <div className='flex w-full my-2 cursor-pointer' key={task.id}>
                <button className='flex w-full' onClick={() => handleSendIDTask(task.id)}>
                  <div className='flex w-full rounded-t-lg'>

                    {/* task name */}
                    <div className='w-4/12 flex items-center '>
                      <div className='w-8 h-8 rounded-lg bg-ts-blue-200 flex items-center justify-center font-bold uppercase'><p>{task.name.substr(0, 1)}</p></div>
                      <p>{task.name}</p>
                    </div>

                    {/* task progess */}
                    <div className="w-5/12 flex items-center ">
                      <div className="overflow-hidden w-28 h-2 text-xs flex rounded bg-gray-300">
                          <div style={{ width: `50%` }} className="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-green-500"></div>
                      </div>
                      <div className="ml-2 flex items-center">
                          <p className="text-gray-400"> 50% </p>
                      </div>
                    </div>

                    {/* project member */}
                    <div className="w-3/12 flex items-center ">
                      <div className='flex col-span-1 items-center justify-center'>
                        <div className="-space-x-2">
                          {project.projectMembers.map((el,index)=>{
                            if(index <= 2){
                            return(
                                <img className="z-30 inline object-cover w-8 h-8 border-2 border-white rounded-full" src={el.profile.picture} alt="Profile image" />
                            )}})
                          }
                        </div>
                      </div>
                      <p className="flex items-center ">
                        {project.projectMembers.length > 3 ? `+${project.projectMembers.length - 3} people` : ""}
                      </p>

                    </div>

                    <div className='flex items-center justify-center'>
                      <DotsVerticalDropDown prefix={"task"} id={0} newOption={false} />
                    </div>

                  </div>
                </button>
              </div>

            )
          }) :
            (<div className='my-2 cursor-pointer'>
              <div className='px-4 py-2 rounded-t-lg'>
                <div className='col-span-4 flex justify-center items-center space-x-2'>
                  <p>---Empty---</p>
                </div>
              </div>
            </div>)}
        </div>

      </div>
      <TaskCreateForm open={showCreateForm} onClose={() => setShowCreateForm(false)} projectData={project}
        onSave={onSaveTask}
      />
    </div >
  )
}

export default myproject
myproject.Layout = MainLayout;
myproject.current = 'project'
myproject.title = 'Project'
myproject.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}