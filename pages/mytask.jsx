import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'

const mytask = () => {
  return (
    <div>
      
    </div>
  )
}

export default mytask
mytask.Layout = MainLayout;
mytask.current = 'mytask'
mytask.title = 'My Task'
mytask.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}