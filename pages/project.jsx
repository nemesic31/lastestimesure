import { useState, useContext, useEffect } from 'react'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import HeaderBox from '../components/HeaderBox'
import ButtonSort from '../components/ButtonSort'
import ProjectCreateForm from '../components/ProjectCreateForm'
import ProjectViewFrom  from '../components/ProjectViewFrom'
import DotsVerticalDropDown from '../components/DotsVerticalDropDown'
import Link from 'next/link'
import { useSession } from "next-auth/client"


import Notification from '../components/layout/notification/Notification'
import { NotificationContext } from '../components/layout/notification/notificationContext'
import { v4 } from 'uuid'

import {
  DotsVerticalIcon,
  UsersIcon
} from '@heroicons/react/solid'
import {
  FolderAddIcon
} from '@heroicons/react/outline'
import Pagination from '../components/layout/Pagination'
import { useRouter } from 'next/router'



const project = () => {
  const [session, loading] = useSession()
  const router = useRouter()
  const [showCreateForm, setShowCreateForm] = useState(false)
  const [dataProject, setDataProject] = useState()
  const [showViewForm, setShowViewForm] = useState(false)
  const [projects, setProjects] = useState([])
  const [projectLoading, setProjectLoading] = useState(false)
  const [sortBy, setSortBy] = useState('createdDate')
  const [sortDirection, setSortDirection] = useState('desc')
  const [pageNumber, setPageNumber] = useState(1)
  const [pageSize, setPageSize] = useState(5)
  const [totalPage, setTotalPage] = useState(0)
  const [totalProject, setTotalProject] = useState(0)
  const [availableMember, setAvailableMember] = useState([])
  const [availableGroup, setAvailableGroup] = useState([])
  const { state, dispatch } = useContext(NotificationContext);

  useEffect(() => {
    loadProjects(pageSize, pageNumber, sortBy, sortDirection)
    loadAvailableMembers()
    loadAvailableGroup()
  }, [])

  const loadAvailableMembers = async () => {
    const url = `/api/profile/`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setAvailableMember(data)
    } catch (error) {
      console.error(error)
    }
  }
  const loadAvailableGroup = async () => {
    const url = `/api/group/getallgroup`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setAvailableGroup(data.content)
    } catch (error) {
      console.error(error)
    }
  }

  const onSaveProject = async (data) => {
    loadProjects(pageSize, pageNumber, sortBy, sortDirection)
    dispatch({
      type: "ADD_NOTIFICATION",
      payload: {
        id: v4(),
        type: 'SUCCESS',
        title: "success",
        message: "Seccessfully saved",
      },
    })
    setShowCreateForm(false)
  }
  const loadProjects = async (_pageSize, _pageNumber, _sortBy, _sortDirection) => {
    setProjectLoading(true)
    const url = `/api/project/listandcreateproject?page_size=${_pageSize}&page=${_pageNumber}&sort_by=${_sortBy}&sort_direction=${_sortDirection}`
    try {
      const result = await fetch(url, { method: 'get' })
      const data = await result.json()
      setTotalPage(data.totalPages)
      setProjects(data.content)
      setTotalProject(data.totalElements)
    } catch (error) {
      console.error(error)
    }
    setProjectLoading(false)
  }

  const onSort = (_sortBy, _sortDirection) => {
    setSortBy(_sortBy)
    setSortDirection(_sortDirection)
    loadProjects(pageSize, pageNumber, _sortBy, _sortDirection)
  }

  const onChangePage = (_pageNumber) => {
    setPageNumber(_pageNumber)
    loadProjects(pageSize, _pageNumber, sortBy, sortDirection)
  }

  const handleSendIDProject = (id) => {
    router.push({
      pathname: `/myproject`,
      query: {
        id: id
      }
    })
  }

  const handleViewProject = (data) => {
    if (data) {
      setDataProject(data)
      setShowViewForm(true)
    }
    if (!data) {
      setDataProject()
      setShowViewForm(false)
    }
  }

  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
      <HeaderBox title="Project" content={totalProject + ' project(s)'} />

      {session?.user?.admin && <div className='flex justify-end px-5 space-x-2'>
        <button className='button-default' onClick={() => setShowCreateForm(true)}>
          <FolderAddIcon className='w-5 h-5' />
          <p>New Project</p>
        </button>
      </div>}

      <div className="rounded-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 divide-y divide-divider">
        <div className='flex px-8 py-2'>
          <div className='w-1/6 flex px-4 items-center font-bold'>Project Name</div>
          <div className='w-2/6 flex items-center justify-center font-bold'>Project Progress</div>
          <div className='w-1/6 flex items-center font-bold justify-center'>Project Status</div>
          <div className='w-1/6 flex items-center justify-center font-bold '>Member</div>
          <div className='w-1/6 flex justify-center z-10'><ButtonSort sortBy={sortBy} sortDirection={sortDirection} onSort={onSort} /></div>
        </div>

        <div className='divide-y divide-divider px-8 flex flex-col w-full'>
          {projectLoading && (
            <div className='flex justify-center mt-2'>
              <div className='indicator'></div>
            </div>
          )}
          {projects.length === 0 && !projectLoading && (
            <>
              <div className='text-center text-sm italic text-ts-white-400 mt-2'>empty</div>
            </>
          )}

          {projects.map((project) => (
            <div className='my-2 flex px-8 py-2' key={project.id}>

              <div className='w-1/6 flex items-center'>
                <button onClick={() => handleSendIDProject(project.id)} className="flex items-center cursor-pointer" type="button">
                  <div className='w-8 h-8 px-3 mr-2 rounded-lg bg-ts-blue-200 flex items-center justify-center font-bold uppercase'><p>{project.name && project.name.substr(0, 1)}</p></div>
                  <p className="truncate ...">{project.name} </p>
                </button>
              </div>

              <div className="w-2/6 flex items-center justify-center">
                <div className="overflow-hidden w-28 h-2 text-xs flex rounded bg-gray-300">
                  <div style={{ width: `50%` }} className="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-green-500"></div>
                </div>
                <div className="ml-2 flex items-center">
                  <p className="text-gray-400"> 50% </p>
                </div>
              </div>

              <div className='w-1/6 flex items-center justify-center'>Underconstruction</div>

              <div className="w-1/6 flex items-center justify-center">
                <p className="flex items-center justify-center">
                  {project.projectMembers.length} people
                </p>
                <p className="flex justify-start"><UsersIcon className='h-4 w-4' /></p>
              </div>



              <div className='w-1/6 flex items-center justify-center rounded-lg p-1 cursor-pointer'>
                <DotsVerticalDropDown prefix={"project"} id={project.id} viewProject={handleViewProject} newOption={false} manageUserOption={true} />
              </div>

            </div>
          ))}
        </div>
        <Pagination currentPage={pageNumber} totalPage={totalPage} onChangePage={onChangePage} />
      </div>
      <ProjectViewFrom open={showViewForm} onClose={() => setShowViewForm(false)} dataProject={dataProject}/>
      <ProjectCreateForm open={showCreateForm} onClose={() => setShowCreateForm(false)} onSave={onSaveProject} availableMember={availableMember} availableGroup={availableGroup}  />
      <Notification />
    </div>
  )
}

export default project
project.Layout = MainLayout;
project.current = 'project'
project.title = 'Project'
project.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}