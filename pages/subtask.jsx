import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'
import Kanban from "../components/kanban"

const subtask = () => {
  const router = useRouter()
  const task_id = router.query.id
  const {project_id} =router.query
  const [allSubtasks, setAllSubtasks] = useState()
  const [taskDataPack, setTaskDataPack] = useState()

  const getKanbanByTaskID = async () => {
    try {
      const response = await fetch(`api/task/viewkanbanbytaskid?task_id=${task_id}`)
      const data = await response.json()
      setAllSubtasks(data)
    } catch (error) {
      console.log(error);
    }
  }

  const getTaskById = async () => {
    try {
      const response = await fetch(`api/task/viewtaskbyid?task_id=${task_id}`)
      const data = await response.json()
      setTaskDataPack(data)
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getTaskById()
   getKanbanByTaskID()
  }, [task_id])

  return (
    <div className="flex flex-col ">
      <div className="bg-white rounded-md flex justify-between py-4 items-center">
        <div className="ml-11 text-3xl font-medium">
        {taskDataPack&&taskDataPack.project.projectGroup.name} - {taskDataPack&&taskDataPack.project.name}
        </div>
        <div className="mr-6 flex font-normal text-base text-ts-gray-900">
            <div className='flex col-span-1 items-center justify-center'>
                <div className="-space-x-2">
                     {taskDataPack && taskDataPack.project.projectMembers.map((el,index)=>{
                            if(index <= 2){
                            return(
                                <img className="z-30 inline object-cover w-8 h-8 border-2 border-white rounded-full" src={el.profile.picture} alt="Profile image" />
                            )}})
                      }
                </div>
            </div>
            <p className="flex items-center ">
                {taskDataPack && taskDataPack.project.projectMembers.length > 3 ? `+${taskDataPack.project.projectMembers.length - 3} people` : ""}
             </p>
        </div>
      </div>
      <div className="bg-white rounded-md shadow-md mt-5 py-6 w-full h-full ">
        <Kanban subtasks={allSubtasks&&allSubtasks} taskid={task_id} 
        taskdata={
          {
            taskId:task_id,
            taskName:taskDataPack&&taskDataPack.name,
            groupName:taskDataPack&&taskDataPack.project.projectGroup.name,
            projectName:taskDataPack&&taskDataPack.project.name
            }}
           projectId={project_id} 
            />
      </div>
    </div>
  )
}

export default subtask
subtask.Layout = MainLayout;
subtask.current = 'subtask'
subtask.title = 'Sub Task'
subtask.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}