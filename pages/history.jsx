import React from 'react'
import { useState, useEffect } from 'react'
import HeaderBox from '../components/HeaderBox'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import { Menu } from '@headlessui/react'
import { Fragment } from 'react'
import { Transition } from '@headlessui/react'
import { classNames } from '../utilities/css-util'
import {
  SwitchVerticalIcon,
  SearchIcon,
} from '@heroicons/react/solid'
import Pagination from '../components/layout/Pagination'
import { useRouter } from 'next/router'

const mock_history = [
  { id: "1", Date: "09/06/2021 16:42:07", Group: "PEA", Project_name: "Inno Platform", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "2", Date: "09/06/2021 16:42:07", Group: "MEA", Project_name: "Smart Grid", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "3", Date: "09/06/2021 16:42:07", Group: "MEA", Project_name: "Smart Inno", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "4", Date: "09/06/2021 16:42:07", Group: "DMS", Project_name: "GFE", Task: "BD OP-41  กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "5", Date: "09/06/2021 16:42:07", Group: "Enter", Project_name: "Website", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "6", Date: "09/06/2021 16:42:07", Group: "MEA", Project_name: "Smart Inno", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "7", Date: "09/06/2021 16:42:07", Group: "DMS", Project_name: "GFE", Task: "BD OP-41  กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "8", Date: "09/06/2021 16:42:07", Group: "Enter", Project_name: "Website", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "9", Date: "09/06/2021 16:42:07", Group: "MEA", Project_name: "Smart Grid", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
  { id: "10", Date: "09/06/2021 16:42:07", Group: "MEA", Project_name: "Smart Inno", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "08/09/21 10:10:23", Done_at: "09/09/21 10:10:23" },
]

const history = () => {
  const router = useRouter()
  const [sortBy, setSortBy] = useState('updatedDate')
  const [historyLoading, setHistoryLoading] = useState(false)
  const [historys, setHistorys] = useState([])
  const [pageNumber, setPageNumber] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [totalPage, setTotalPage] = useState(3)
  const [search, setSearch] = useState("")

  useEffect(() => {
    loadHistorys(search, pageSize, pageNumber, sortBy)
  }, [])

  const loadHistorys = async (_search, _pageSize, _pageNumber, _sortBy) => {
    setHistoryLoading(true)
    const url = `/api/history/history?page_size=${_pageSize}&page=${_pageNumber}&sort_by=${_sortBy}&sort_direction=desc`
    try {
      const result = await fetch(url, {
        method: 'post',
        body: JSON.stringify(
          {
            "sinceDate": "",
            "toDate": "",
            "searchKeyword": `${_search}`,
            "projectId": ""
          }
        )
      })
      const data = await result.json()
      setTotalPage(data.totalPages)
      setHistorys(data.content)
    } catch (error) {
      console.error(error)
      setHistorys(mock_history)
    }
    setHistoryLoading(false)
  }

  const isoToUtc = (isoString) => {
    var utcDate = new Date(isoString).toLocaleDateString('en-GB')
    var utcTime = new Date(isoString).toLocaleTimeString('en-GB')
    return [utcDate, utcTime]
  }

  const onChangePage = (_pageNumber) => {
    setPageNumber(_pageNumber)
    loadHistorys(search, pageSize, _pageNumber, sortBy)
  }

  const onSort = (_sortBy, _sortDirection) => {
    setSortBy(_sortBy)
    loadHistorys(search, pageSize, pageNumber, _sortBy)
  }

  const onSearchSubmit = event => {
    event.preventDefault()
    loadHistorys(search, pageSize, pageNumber, sortBy)
    console.log(search)
  }

  const handleSendIDHistory = (id) => {
    router.push({
      pathname: `/manage_history`,
      query: {
        id: id
      }
    })
  }

  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
      <HeaderBox title='History' />
      <div className='flex justify-end px-5 space-x-5'>
        <form onSubmit={onSearchSubmit}>
          <div className='relative flex items-center'>
            <SearchIcon className="w-5 h-5 absolute ml-3" />
            <input className="rounded-full pr-20 pl-10 py-2" placeholder="ค้นหา" value={search} onChange={e => setSearch(e.target.value)} />
          </div>
        </form>

        <Menu as="div" className="ml-5 relative">
          <div>
            <Menu.Button className="order-1 inline-flex items-center p-2 border border-gray-300 shadow-sm rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none sm:order-0 sm:ml-0">
              <SwitchVerticalIcon
                className="flex-shrink-0 h-4 text-gray-400"
                aria-hidden="true"
              />
              <span className="ml-2 text-gray-700 text-sm font-medium">
                Sort by
              </span>
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="text-left origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
              <Menu.Item>
                {({ active }) => (
                  <a

                    className={classNames(active || (sortBy === 'createdDate') ? 'bg-gray-200' : '', 'block px-3 text-sm text-gray-600 mx-1 mt-1 rounded-lg cursor-pointer')}
                    onClick={() => onSort('createdDate')}
                  >
                    Create Date
                  </a>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <a
                    className={classNames(active || (sortBy === 'updatedDate') ? 'bg-gray-200' : '', 'block px-3 text-sm text-gray-600 mx-1 mt-1 rounded-lg cursor-pointer')}
                    onClick={() => onSort('updatedDate')}
                  >
                    Done Update
                  </a>
                )}
              </Menu.Item>
            </Menu.Items>

          </Transition>
        </Menu>

      </div>

      <div className="bg-white divide-y divide-divider rounded-md shadow-md mt-5 py-3 w-full h-full">
        <div className="divide-y divide-divider w-full h-full pb-5 overflow-x-scroll">

          <div className='inline-flex px-12 py-2 space-x-2'>
            <div className='w-44 items-center justify-center font-bold'>Lastest Update</div>
            <div className='w-28 items-center justify-start font-bold'>Group</div>
            <div className='w-32 items-center justify-start font-bold'>Project Name</div>
            <div className='w-72 items-center justify-start font-bold'>Task Name</div>
            <div className='w-32 items-center justify-start font-bold'>Subtask Name</div>
            <div className='w-40 items-center justify-start font-bold'>Start at</div>
            <div className='w-40 items-center justify-start font-bold'>Done at</div>
          </div>

          <div className='divide-y divide-divider px-4 w-full'>
            {historyLoading && (
              <div className='flex justify-center mt-2'>
                <div className='indicator'></div>
              </div>
            )}
            {historys.length === 0 && !historyLoading && (
              <>
                <div className='text-center text-sm italic text-ts-white-400 mt-2'>empty</div>
              </>
            )}

            {historys && historys.map((history) => (
              <div onClick={() => handleSendIDHistory(history.id)} className='inline-flex px-8 py-5 space-x-2 cursor-pointer' key={history.id}>
                <div className='w-44 items-center justify-start'>{isoToUtc(history.updatedDate)[0]} {isoToUtc(history.updatedDate)[1]}</div>
                <div className='w-28 items-center justify-start truncate'>{history.projectGroup.name}</div>
                <div className='w-32 items-center justify-start truncate'>{history.project.name}</div>
                <div className='w-72 items-center justify-start truncate'>{history.task.name}</div>
                <div className='w-32 items-center justify-start truncate'>{history.subtask.name}</div>
                <div className='w-40 items-center justify-start'>{isoToUtc(history.startTime)[0]} <span className='text-gray-500'>{isoToUtc(history.startTime)[1]}</span></div>
                <div className='w-40 items-center justify-start'>{isoToUtc(history.stopTime)[0]} <span className='text-gray-500'>{isoToUtc(history.stopTime)[1]}</span></div>
              </div>
            ))}
          </div>

        </div>
        <Pagination currentPage={pageNumber} totalPage={totalPage} onChangePage={onChangePage} />
      </div>

    </div>
  )
}

export default history
history.Layout = MainLayout
history.current = 'history'
history.title = 'History'
history.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}