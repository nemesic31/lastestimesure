import { useState, useContext } from 'react'
import MainLayout from '../components/MainLayout'
import { getSession } from 'next-auth/client'
import HeaderBox from '../components/HeaderBox'
import ButtonSort from '../components/ButtonSort'
import { classNames } from '../utilities/css-util'
import GroupCreateForm from '../components/GroupCreateForm'
import GroupViewForm from '../components/GroupViewForm'
import ProjectCreateForm from '../components/ProjectCreateForm'
import DotsVerticalDropDown from '../components/DotsVerticalDropDown'
import { useEffect } from 'react'
import Link from 'next/link'
import { useSession } from "next-auth/client"


import Notification from '../components/layout/notification/Notification'
import { NotificationContext } from '../components/layout/notification/notificationContext'
import { v4 } from 'uuid'

import {
  DotsVerticalIcon,
  ChevronRightIcon,
  ChevronDownIcon,
  UsersIcon
} from '@heroicons/react/solid'
import {
  FolderAddIcon
} from '@heroicons/react/outline'
import Pagination from '../components/layout/Pagination'
import { useRouter } from 'next/router'


const group = () => {
  const [session, loading] = useSession()
  const router = useRouter()
  const [openGroup, setOpenGroup] = useState({})
  const [dataGroup, setDataGroup] = useState()
  const [showCreateForm, setShowCreateForm] = useState(false)
  const [showViewForm, setShowViewForm] = useState(false)
  const [showProjectCreateForm, setShowProjectCreateForm] = useState(false)
  const [groups, setGroups] = useState([])
  const [groupLoading, setGroupLoading] = useState(false)
  const [sortBy, setSortBy] = useState('createdDate')
  const [sortDirection, setSortDirection] = useState('desc')
  const [pageNumber, setPageNumber] = useState(1)
  const [pageSize, setPageSize] = useState(5)
  const [totalPage, setTotalPage] = useState(0)
  const [totalGroup, setTotalGroup] = useState(0)
  const [availableMember, setAvailableMember] = useState([])
  const [availableGroup, setAvailableGroup] = useState([])
  const { state, dispatch } = useContext(NotificationContext);

  const handleSendIDProject = (id) => {
    router.push({
      pathname: `/myproject`,
      query: {
        id: id
      }
    })
  }

  useEffect(() => {
    loadGroups(pageSize, pageNumber, sortBy, sortDirection)
    loadAvailableMembers()
    loadAvailableGroup()
  }, [])

  const loadAvailableMembers = async () => {
    const url = `/api/profile/`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setAvailableMember(data)
    } catch (error) {
      console.error(error)
    }
  }

  const loadAvailableGroup = async () => {
    const url = `/api/group/getallgroup`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setAvailableGroup(data.content)
    } catch (error) {
      console.error(error)
    }
  }

  const onSaveGroup = async (data) => {
    loadGroups(pageSize, pageNumber, sortBy, sortDirection)
    dispatch({
      type: "ADD_NOTIFICATION",
      payload: {
        id: v4(),
        type: 'SUCCESS',
        title: "success",
        message: "Seccessfully saved",
      },
    })
    setShowCreateForm(false)
  }
  const onSaveProject = async (data) => {
    loadGroups(pageSize, pageNumber, sortBy, sortDirection)
    dispatch({
      type: "ADD_NOTIFICATION",
      payload: {
        id: v4(),
        type: 'SUCCESS',
        title: "success",
        message: "Seccessfully saved",
      },
    })
    setShowProjectCreateForm(false)
  }

  const loadGroups = async (_pageSize, _pageNumber, _sortBy, _sortDirection) => {
    setGroupLoading(true)
    const url = `/api/group/listandcreategroup?page_size=${_pageSize}&page=${_pageNumber}&sort_by=${_sortBy}&sort_direction=${_sortDirection}`
    try {
      const result = await fetch(url, { method: 'get' })
      const data = await result.json()
      setTotalPage(data.totalPages)
      setGroups(data.content)
      setTotalGroup(data.totalElements)
    } catch (error) {
      console.error(error)
    }
    setGroupLoading(false)
  }

  const toggleOpenGroup = (groupId) => {
    const _openGroup = { ...openGroup }
    if (_openGroup[groupId])
      delete _openGroup[groupId]
    else
      _openGroup[groupId] = true

    setOpenGroup(_openGroup)
  }

  const handleViewGroup = (data) => {
    if (data) {
      setDataGroup(data)
      setShowViewForm(true)
    }
    if (!data) {
      setDataGroup()
      setShowViewForm(false)
    }
  }

  const handleCreateProjectByGroupID = (data) => {
    if (data) {
      setDataGroup(data)
      setShowProjectCreateForm(true)
    }
    if (!data) {
      setDataGroup()
      setShowProjectCreateForm(false)
    }
  }

  const onSort = (_sortBy, _sortDirection) => {
    setSortBy(_sortBy)
    setSortDirection(_sortDirection)
    loadGroups(pageSize, pageNumber, _sortBy, _sortDirection)
  }

  const onChangePage = (_pageNumber) => {
    setPageNumber(_pageNumber)
    loadGroups(pageSize, _pageNumber, sortBy, sortDirection)
  }



  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
      <HeaderBox title='Group' content={totalGroup + ' group(s)'} />

      {session?.user?.admin && <div className='flex justify-end px-5 space-x-2'>
        <button className='button-default' onClick={() => setShowCreateForm(true)}>
          <FolderAddIcon className='w-5 h-5' />
          <p>New group</p>
        </button>
      </div>}

      <div className="rounded-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 divide-y divide-divider">

        <div className='grid grid-cols-9 px-8 py-2'>
          <div className='col-span-6 flex items-center font-bold'>Group Name</div>
          <div className='flex items-center font-bold justify-center'>Project</div>
          <div className='flex items-center font-bold justify-center'>Member</div>
          <div className='flex justify-center z-10'><ButtonSort sortBy={sortBy} sortDirection={sortDirection} onSort={onSort} /></div>
        </div>

        <div className='divide-y divide-divider px-8'>
          {groupLoading && (
            <div className='flex justify-center mt-2'>
              <div className='indicator'></div>
            </div>
          )}
          {groups.length === 0 && !groupLoading && (
            <>
              <div className='text-center text-sm italic text-ts-white-400 mt-2'>empty</div>
            </>
          )}
          {groups.map((item) => (
            <div className='my-2' key={item.id}>
              <div
                className={classNames(
                  openGroup[item.id] ? 'bg-ts-aquagreen-100' : '',
                  'grid grid-cols-9 px-4 py-2 rounded-t-lg'
                )}>
                <div className='col-span-6 flex items-center space-x-2'>
                  <div className='w-8 h-8 rounded-lg bg-ts-blue-200 flex items-center justify-center font-bold uppercase'><p>{item.name && item.name.substr(0, 1)}</p></div>
                  <p>{item.name}</p>
                </div>
                <div className='flex items-center justify-center'>{item.projects.length} Project(s)</div>
                <div className='flex items-center'></div>
                <div className='flex justify-between items-center'>
                  <div className='hover:bg-gray-300 rounded-lg p-1 cursor-pointer'>
                    <DotsVerticalDropDown prefix={"group"} id={item.id} newOption={true} manageUserOption={false} handleCreateProject={handleCreateProjectByGroupID} viewGroup={handleViewGroup} />
                  </div>
                  <div onClick={() => toggleOpenGroup(item.id)} className='hover:bg-gray-300 rounded-lg p-1 cursor-pointer'>
                    {!openGroup[item.id] && <ChevronRightIcon className='w-6 h-6' />}
                    {openGroup[item.id] && <ChevronDownIcon className='w-6 h-6' />}
                  </div>
                  <div></div>
                </div>
              </div>
              <div className='bg-inputbox rounded-b-lg'>
                {openGroup[item.id] && item.projects.length === 0 && (
                  <>
                    <div className='text-center text-sm italic text-ts-white-400 py-2'>empty</div>
                  </>
                )}
                {openGroup[item.id] && item.projects.map((project) => (
                  <button type="button" key={project.id} onClick={() => handleSendIDProject(project.id)} className="w-full">
                    <div className='grid grid-cols-9 px-4 py-2  hover:bg-gray-200' key={project.id}>
                      <p className='col-span-6 flex items-center space-x-2 pl-14'>{project.name}</p>
                      <div className='flex items-center'></div>
                      <div className='flex items-center justify-center space-x-1'><p>{project.projectMembers.length}</p><p><UsersIcon className='h-4 w-4' /></p></div>
                      <div className='flex items-center'></div>
                    </div>
                  </button>
                ))}
              </div>

            </div>
          ))}
        </div>

        <Pagination currentPage={pageNumber} totalPage={totalPage} onChangePage={onChangePage} />
      </div>
      <GroupViewForm open={showViewForm} onClose={() => setShowViewForm(false)} dataGroup={dataGroup} />
      <GroupCreateForm open={showCreateForm} onClose={() => setShowCreateForm(false)} onSave={onSaveGroup} />
      <ProjectCreateForm id={dataGroup && dataGroup.id} open={showProjectCreateForm} onClose={() => setShowProjectCreateForm(false)} onSave={onSaveProject} availableMember={availableMember} availableGroup={availableGroup} dataGroup={dataGroup} />
      <Notification />
    </div >
  )
}

export default group
group.Layout = MainLayout;
group.current = 'group'
group.title = 'Group'
group.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}