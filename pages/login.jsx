import { providers, signIn, getSession } from "next-auth/client";

const login = ({ providers }) => {
  return (
    <div className="min-h-screen bg-white flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className=" sm:mx-auto sm:w-full sm:max-w-md ">
        <div>
          <img
            className="mx-auto w-auto h-44"
            src="logo-verical.png"
          />
            <div>
              <button
                onClick={() => signIn(providers.google.id)}
                type="button"
                className="w-full mt-4 inline-flex space-x-2 justify-center items-center py-1 px-4 border border-gray-400 rounded-md bg-white hover:bg-gray-200 text-sm leading-5 font-medium text-gray-500 focus:outline-none focus:border-blue-300 transition duration-150 ease-in-ou"
              >
                <img className="h-7 w-7" src="/google_g_icon.png" />
                <div>Login with Google</div>
              </button>
            </div>
        </div>
      </div>
    </div>
  )
}

export default login
login.title = 'Login'

login.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (session) {
    res.writeHead(302, {
      Location: '/timesheet'
    })
    res.end()
    return
  }
  return {
    session: undefined,
    providers: await providers(context)
  }
}