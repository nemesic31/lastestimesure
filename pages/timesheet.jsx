import React from 'react'
import { useState, useEffect } from 'react'
import HeaderBox from '../components/HeaderBox'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import {
    PauseIcon,
    PlayIcon,
    StarIcon,
    StopIcon,
} from '@heroicons/react/outline'
const mock_tasks = [
    { name: "ปิด Sprint #6", project: "PEA - Inno Platform", task: "ประชุม", status: "continue", start_on: "18/08/2021 11:02:43" },
    { name: "แก้ไขหน้าปกเอกสาร", project: "PEA - Inno Platform", task: "BD OP-47 ทำเอกสารในการอบรมระบบ", status: "pause", start_on: "18/08/2021 11:02:43" },
    { name: "ออกแบบ", project: "MEA - EV", task: "BD OP-47 ทำเอกสารในการอบรมระบบ", status: "pause", start_on: "18/08/2021 11:02:43" },
]

const timesheet = () => {
    const [tasks, setTasks] = useState(mock_tasks)
    const [timesheetPack, setTimesheetPack] = useState()
    const [taskLoading, setTaskLoading] = useState(false)
    const [statusFocus, setStatusFocus] = useState(true)

    const toggleStatus = (e, taskname) => {
        let tempTasks = tasks
        tempTasks.forEach(task => {
            if (task.name === taskname) {
                if (task.status === 'continue') {
                    task.status = 'pause'
                } else if (task.status === 'pause') {
                    task.status = 'continue'
                }
            }

        })
        setTasks(tempTasks)
    }
    const loadTasks = async () => {
        setTaskLoading(true)
        setTaskLoading(false)
    }

    const getTimesheetByUser = async () => {
        try {
          const response = await fetch(`api/timesheet/getalltimesheetbyuser`)
          const data = await response.json()
          setTimesheetPack(data)
        } catch (error) {
          console.log(error);
        }
      }

    const handleChangeStatus = async (value) =>{
        const {status,subtask_id}=value
        try {
            const response = await fetch(`api/subtask/dndcard?subtask_id=${subtask_id}&to_be_status=${status}`)
            const data = await response.json()
            if (data) {
              getKanbanByTaskID()
            }
          } catch (error) {
            console.log(error);
          }
        getTimesheetByUser()
    }

    const handleStatusButton = async (value) =>{
        const {status} = value
        if(status === 'todo'){
            setStatusFocus(false)
        }

        if(status === 'doing'){
            setStatusFocus(true)
        }

    }

    useEffect(() => {
        getTimesheetByUser()
    }, [])


    return (
        <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
            <HeaderBox title='Timesheet' />
            <div>
            <div className="flex -space-x-2">
                <button onClick={()=>handleStatusButton({status:'todo'})} className={`inline  px-6 py-1 rounded-t-xl font-semibold ${!statusFocus ? 'z-40 bg-yellow-400':'z-20 bg-gray-300'}`}>Todo</button>
                <button onClick={()=>handleStatusButton({status:'doing'})} className={`z-30 inline  px-6 py-1 rounded-t-xl font-semibold ${statusFocus ? 'bg-green-400':'bg-gray-300'}`}>Doing</button>
            </div>
        {statusFocus ? <div className="h-screen flex flex-col rounded-b-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 overflow-y-scroll">
                {timesheetPack && timesheetPack.map((task) => (
                    <div key={task.id} className='h-auto rounded-xl border border-gray-400 mt-4 mb-4 mx-20 shadow-xl'>
                        <div className='grid grid-cols-6 mt-5 mb-5 mx-10 gap-4'>
                            <div className='col-span-3 text-xl font-bold leading-7'>{task.subtask.name}</div>
                            <div className='col-span-3 justify-self-end flex space-x-5'>
                                <button class="bg-red-400 hover:bg-red-600 text-black font-bold py-2 px-4 rounded-full border border-gray-400 inline-flex items-center space-x-2"
                                        
                                    >
                                        <PauseIcon className='w-5 h-5' />
                                        <p>Pause</p>
                                    </button>
                                <button onClick={()=>handleChangeStatus({status:'done',subtask_id:task.subtask.id})} class=" bg-gray-200 hover:bg-gray-600 text-black font-bold py-2 px-4 rounded-full border border-gray-400 inline-flex items-center space-x-2">
                                    <StopIcon className='w-5 h-5' />
                                    <span>Done</span>
                                </button>
                            </div>
                            <div className='col-span-3 text-sm italic text-ts-white-400'>{task.projectGroup.name} - {task.project.name} / {task.task.name}</div>
                            <div className='col-span-3 justify-self-end text-sm italic text-ts-white-400'>Start on: {new Date(`${task.createdDate}`).toLocaleDateString()}</div>
                        </div>

                    </div>
                ))
                }

                


            </div>:
            <div className="h-screen rounded-b-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 overflow-y-scroll">
            {timesheetPack && timesheetPack.map((task) => (
                <div key={task.id} className='h-auto rounded-xl border border-gray-400 mt-4 mb-4 mx-20 shadow-xl'>
                    <div className='grid grid-cols-6 mt-5 mb-5 mx-10 gap-4'>
                        <div className='col-span-3 text-xl font-bold leading-7'>{task.subtask.name}</div>
                        <div className='col-span-3 justify-self-end flex space-x-5'>
                            <button class="bg-green-400 hover:bg-green-600 text-black font-bold py-2 px-4 rounded-full border border-gray-400 inline-flex items-center space-x-2"
                                    
                                >
                                    <PlayIcon className='w-5 h-5' />
                                    <p>Start</p>
                                </button>
                           
                        </div>
                        <div className='col-span-3 text-sm italic text-ts-white-400'>{task.projectGroup.name} - {task.project.name} / {task.task.name}</div>
                        <div className='col-span-3 justify-self-end text-sm italic text-ts-white-400'>Start on: {new Date(`${task.createdDate}`).toLocaleDateString()}</div>
                    </div>

                </div>
            ))
            }

            


        </div>
            }
            </div>
        </div>
    )
}


export default timesheet
timesheet.Layout = MainLayout;
timesheet.current = 'timesheet'
timesheet.title = 'Timesheet'
timesheet.getInitialProps = async (context) => {
    const { req, res } = context
    const session = await getSession({ req })
    if (!session) {
        res.writeHead(302, {
            Location: '/login'
        })
        res.end()
        return
    }
    return {
        user: session
    }
}