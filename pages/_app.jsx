import 'tailwindcss/tailwind.css'
import { Provider } from 'next-auth/client'
import Head from 'next/head'
import '../styles/index.css'
import { NotificationContextProvider } from '../components/layout/notification/notificationContext'

function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout || emptyLayout;
  const current = Component.current || '';
  const title = Component.title || '';

  return (
    <Provider session={pageProps.session}>
      <Layout current={current}>
        <Head>
          <title>TimeSure{title ? ' - ' + title : ''}</title>
          <link rel="shortcut icon" href="/logo-icon.ico" />
        </Head>
        <NotificationContextProvider>
          <Component {...pageProps} />
        </NotificationContextProvider>
      </Layout>
    </Provider>
  )
}

export default MyApp
const emptyLayout = ({ children }) => <>{children}</>