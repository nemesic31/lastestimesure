import { subtaskMock } from '../../../../data/subtaskMock.js'

const handler = async (request, response) => {
  if (request.method === 'POST') {
    const { idCard, idSubtask, newValue, idComment } = request.body
    const indexSubtask = subtaskMock[idCard].items.findIndex(element => element.id === idSubtask);
    subtaskMock[idCard].items[indexSubtask].comment[idComment].description = newValue
    if (subtaskMock[idCard].items[indexSubtask].comment[idComment].description === newValue) {
      response.status(201).json(newValue)
    }


  }
}

export default handler