import { patch } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/subtask`

const handler = async (request, response) => {
  try {
    const { subtask_id,to_be_status } = request.query
    const _url = `${url}/status/user?subtask_id=${subtask_id}&to_be_status=${to_be_status}`
    const result = await patch(request, _url, {})
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

export default handler