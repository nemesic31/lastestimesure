import { get, post } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/subtask`

const handler = async (request, response) => {
  try {
    const { subtask_id } = request.query
    const _url = `${url}/${subtask_id}`
    const result = await get(request, _url, {})
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

export default handler