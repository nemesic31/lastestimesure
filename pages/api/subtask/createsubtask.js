import { post } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/subtask`

const handlerPost = async (request, response) => {
  try {
    const result = await post(request, url, {}, request.body)
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

const handler = async (request, response) => {
    if (request.method === 'POST') {
    response = await handlerPost(request, response)
  } else {
    response.status(404).send()
  }
}

export default handler