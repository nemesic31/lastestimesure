import { patch } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/timesheet`

const handler = async (request, response) => {
    try {
        const _url = `${url}/edit`
        const result = await patch(request, _url, {}, request.body)
        const data = await result.json()
        return Promise.resolve(response.status(200).send(data))
    } catch (error) {
        console.log(error)
        return Promise.resolve(response.status(error.status).send())
    }
}


export default handler