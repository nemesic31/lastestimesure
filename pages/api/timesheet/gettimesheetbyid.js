import { get, post } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/timesheet`

const handler = async (request, response) => {
  try {
    const { timesheet_id } = request.query
    const _url = `${url}/${timesheet_id}`
    const result = await get(request, _url, {})
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

export default handler