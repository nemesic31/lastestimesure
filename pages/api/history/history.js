import { get, post } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/history`

const handlerPost = async (request, response) => {
  try {
    const {page_size, page, sort_by, sort_direction } = request.query
    const _url = `${url}?page_size=${page_size}&page=${page}&sort_by=${sort_by}&sort_direction=${sort_direction}`
    const result = await post(request, _url, {}, request.body)
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    console.log(error)
    return Promise.resolve(response.status(error.status).send())
  }
}

const handler = async (request, response) => {
  if (request.method === 'POST') {
    response = await handlerPost(request, response)
  } else {
    response.status(404).send()
  }
}

export default handler