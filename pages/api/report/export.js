import { post } from "../../../utilities/fetch-util"

const admin_url = `${process.env.BACKEND_URL}/admin/report`

const handlerPost = async (request, response) => {
    try {
        const _url = `${admin_url}/export`
        const result = await post(request, _url, {
            "Content-Type" : "application/octet-stream",
            "Content-Disposition": `attachment;filename="report.xlsx"`
        }, request.body)
        const data = await result.blob()
        return Promise.resolve(response.status(200).send(data))
    } catch (error) {
        console.log(error)
        return Promise.resolve(response.status(error.status).send())
    }
}

const handler = async (request, response) => {
    if (request.method === 'POST') {
        response = await handlerPost(request, response)
    } else {
        response.status(404).send()
    }
}

export default handler