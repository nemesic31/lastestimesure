import { post } from "../../../utilities/fetch-util"

const admin_url = `${process.env.BACKEND_URL}/admin/task`

const handlerPost = async (request, response) => {
  try {
    const result = await post(request, admin_url, {}, request.body)
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

const handler = async (request, response) => {
  if (request.method === 'GET') {
    response = await handlerGet(request, response)
  } else if (request.method === 'POST') {
    response = await handlerPost(request, response)
  } else {
    response.status(404).send()
  }
}

export default handler