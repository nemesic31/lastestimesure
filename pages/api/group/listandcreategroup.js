
import { get, post } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/group`
const admin_url = `${process.env.BACKEND_URL}/admin/group`

const handlerGet = async (request, response) => {
  try {
    const { page_size, page, sort_by, sort_direction } = request.query
    const _url = `${url}?page_size=${page_size}&page=${page}&sort_by=${sort_by}&sort_direction=${sort_direction}`
    const result = await get(request, _url, {})
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

const handlerPost = async (request, response) => {
  try {
    const result = await post(request, admin_url, {}, request.body)
    const data = await result.json()
    return Promise.resolve(response.status(200).send(data))
  } catch (error) {
    return Promise.resolve(response.status(error.status).send())
  }
}

const handler = async (request, response) => {
  if (request.method === 'GET') {
    response = await handlerGet(request, response)
  } else if (request.method === 'POST') {
    response = await handlerPost(request, response)
  } else {
    response.status(404).send()
  }
}

export default handler