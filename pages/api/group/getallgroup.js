import { get } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/group`

const handler = async (request, response) => {
    try {
        const _url = `${url}?sort_by=createdDate&sort_direction=desc`
        const result = await get(request, _url, {})
        const data = await result.json()
        return Promise.resolve(response.status(200).send(data))
      } catch (error) {
        return Promise.resolve(response.status(error.status).send())
      }
}

export default handler