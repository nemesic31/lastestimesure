import { get } from "../../../utilities/fetch-util"

const url = `${process.env.BACKEND_URL}/project`

const handler = async (request, response) => {
    try {
        const _url = `${url}?sort_by=createdDate&sort_direction=desc`
        const result = await get(request, _url, {})
        const data = await result.json()
        return Promise.resolve(response.status(200).send(data))
    } catch (error) {
        console.log(error)
        return Promise.resolve(response.status(error.status).send())
    }
}

export default handler