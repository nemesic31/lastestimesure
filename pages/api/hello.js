import { getToken } from "../../utilities/jwt-util";

const  handler = async (request, response) => {
  const jwt = getToken(request)

  let fetchOption = {
    method: 'get',
    headers: {
      'Authorization': `Bearer ${jwt}`
    }
  };
  const res = await fetch(`${process.env.BACKEND_URL}/hello`, fetchOption);
  if(res.status === 200) {
    const text = await res.text();
    response.status(200).send(text)
  } else {
    response.status(res.status).send()
  }
}

export default handler