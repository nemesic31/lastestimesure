import { get, post } from "../../utilities/fetch-util"


const handlerGet = async (request, response) => {
    try {
      const _url = 'https://api-timesure-dev.entercorp.net/project/availableMembers/'
      const result = await get(request, _url, {})
      const data = await result.json()
      return Promise.resolve(response.status(200).send(data))
    } catch (error) {
      return Promise.resolve(response.status(error.status).send())
    }
  }

  const handler = async (request, response) => {
    if (request.method === 'GET') {
      response = await handlerGet(request, response)
    } else if (request.method === 'POST') {
      response = await handlerPost(request, response)
    } else {
      response.status(404).send()
    }
  }
  
  export default handler