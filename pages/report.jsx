import React from 'react'
import { useState, useEffect, Fragment } from 'react'
import HeaderBox from '../components/HeaderBox'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import { useSession } from "next-auth/client"
import Pagination from '../components/layout/Pagination'
import { Listbox, Transition } from '@headlessui/react'
import Flatpickr from "react-flatpickr";
import "flatpickr/dist/themes/material_blue.css";

import {
  DocumentDownloadIcon,
} from '@heroicons/react/outline'
const mock_history = [
  { id: "1", Date: "09/12/2021 16:42:07", Group: "PEA", Project_name: "Inno Platform", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "09/12/2021 09:10:23", Done_at: "09/12/2021 10:10:23" },
  { id: "2", Date: "10/12/2021 16:42:07", Group: "MEA", Project_name: "Smart Grid", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "10/12/2021 09:10:23", Done_at: "10/12/2021 10:10:23" },
  { id: "3", Date: "11/12/2021 16:42:07", Group: "MEA", Project_name: "Smart Inno", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "11/12/2021 09:10:23", Done_at: "11/12/2021 10:10:23" },
  { id: "4", Date: "12/12/2021 16:42:07", Group: "DMS", Project_name: "GFE", Task: "BD OP-41  กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "12/12/2021 09:10:23", Done_at: "12/12/2021 10:10:23" },
  { id: "5", Date: "13/12/2021 16:42:07", Group: "Enter", Project_name: "Website", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "13/12/2021 09:10:23", Done_at: "13/12/2021 10:10:23" },
  { id: "6", Date: "14/12/2021 16:42:07", Group: "MEA", Project_name: "Smart Inno", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "14/12/2021 09:10:23", Done_at: "14/12/2021 10:10:23" },
  { id: "7", Date: "15/12/2021 16:42:07", Group: "DMS", Project_name: "GFE", Task: "BD OP-41  กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "15/12/2021 09:10:23", Done_at: "15/12/2021 10:10:23" },
  { id: "8", Date: "16/12/2021 16:42:07", Group: "Enter", Project_name: "Website", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "16/12/2021 09:10:23", Done_at: "16/12/2021 10:10:23" },
  { id: "9", Date: "17/12/2021 16:42:07", Group: "MEA", Project_name: "Smart Grid", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "17/12/2021 09:10:23", Done_at: "17/12/2021 10:10:23" },
  { id: "10", Date: "18/12/2021 16:42:07", Group: "MEA", Project_name: "Smart Inno", Task: "BD OP-41 กระบวนการรวมเอกสารส่งมอบ", Subtask: "รวบรวมเอกสาร", Estimated_time: "1 h 30 m", Time_spent: "1 h 0 m", Start_at: "18/12/2021 09:10:23", Done_at: "18/12/2021 10:10:23" },
]

const mock_member = [
  { id: "0", Member_name: "All member" },
  { id: "1", Member_name: "LeBron James" },
  { id: "2", Member_name: "Bronny James" },
  { id: "3", Member_name: "Tracy Mcgredy" },
  { id: "4", Member_name: "Paul George" },
  { id: "5", Member_name: "Kawhi Leonard" },
]

function classNamesProject(...classes) {
  return classes.filter(Boolean).join(' ')
}

function classNamesMember(...classes) {
  return classes.filter(Boolean).join(' ')
}

const report = () => {

  const [session, loading] = useSession()
  const [historyLoading, setHistoryLoading] = useState(false)
  const [historys, setHistorys] = useState([])
  const [pageNumber, setPageNumber] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [totalPage, setTotalPage] = useState(1)
  const [projects, setProjects] = useState()
  const [members, setMembers] = useState()
  const [selectedProject, setSelectedProject] = useState()
  const [selectedMember, setSelectedMember] = useState()
  const [newStartDate, setNewStartDate] = useState()
  const [newEndDate, setNewEndDate] = useState()

  useEffect(() => {
    loadHistorys(pageSize, pageNumber, newStartDate, newEndDate, selectedProject, selectedMember)
    loadAllProjects()
    loadAllMembers()
  }, [pageSize, pageNumber, newStartDate, newEndDate, selectedProject, selectedMember])

  const loadHistorys = async (_pageSize, _pageNumber, _newStartDate, _newEndDate, _selectedProject, _selectedMember) => {
    setHistoryLoading(true)
    const url = `/api/report/report?page_size=${_pageSize}&page=${_pageNumber}&sort_by=updatedDate&sort_direction=desc`
    var pID = selectedProject ? selectedProject.id : null
    var mID = selectedMember ? selectedMember.id : null
    try {
      const result = await fetch(url, {
        method: 'post',
        body: JSON.stringify(
          {
            "sinceDate": newStartDate,
            "toDate": newEndDate,
            "searchKeyword": "",
            "projectId": pID,
            "profileId": mID
          }
        )
      })
      const data = await result.json()
      setTotalPage(data.totalPages)
      setHistorys(data.content)
      setHistoryLoading(false)
    } catch (error) {
      console.error(error)
      setHistorys(mock_history)
    }
    setHistoryLoading(false)
  }

  const onHandlerExportReport = async () => {
    const _url = `/api/report/export`
    var pID = selectedProject ? selectedProject.id : null
    try {
      const result = await fetch(_url, {
        method: 'post',
        body: JSON.stringify(
          {
            "sinceDate": newStartDate,
            "toDate": newEndDate,
            "searchKeyword": "",
            "projectId": pID,
            "profileId": selectedMember
          }
        )
      })
      const data = await result.blob()
      const url = window.URL.createObjectURL(
        new Blob([data]),
        { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }
      );
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute(
        'download',
        `report.xlsx`,
      );
      document.body.appendChild(link);
      link.click();
      link.parentNode.removeChild(link);
    } catch (error) {
      console.error(error)
    }
  }

  const isoToUtc = (isoString) => {
    var utcDate = new Date(isoString).toLocaleDateString('en-GB')
    var utcTime = new Date(isoString).toLocaleTimeString('en-GB')
    return [utcDate, utcTime]
  }

  const loadAllProjects = async () => {
    const url = `/api/project/getallproject`
    try {
      const result = await fetch(url, { method: 'get' })
      const data = await result.json()
      setProjects(data.content)
    } catch (error) {
      console.error(error)
    }
  }

  const loadAllMembers = async () => {
    const url = `/api/profile/`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setMembers(data)
    } catch (error) {
      console.error(error)
    }
  }

  const onChangePage = (_pageNumber) => {
    setPageNumber(_pageNumber)
    loadHistorys(pageSize, _pageNumber)
  }

  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-7">
      <HeaderBox title='Report' />
      <div className='grid grid-cols-10 gap-4 px-10 justify-start'>
        <div className='col-span-3 grid grid-cols-5 inline-flex'>
          <p className='flex w-full col-span-2'>Select Project :</p>

          <Listbox value={selectedProject} onChange={setSelectedProject}>
            <div className="relative mr-3 w-full col-span-3">
              <Listbox.Button className="relative w-full bg-white border border-ts-gray-500 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-0 focus:border-ts-gray-600 sm:text-sm">
                <span className="w-full inline-flex truncate">
                  <span className="truncate">{selectedProject ? selectedProject.name : 'All Project'}</span>
                </span>
                <span className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                  <img src="chevron-down.svg" alt="chevron-down" />
                </span>
              </Listbox.Button>
              <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                  <Listbox.Option
                    key={null}
                    className={({ active }) =>
                      classNamesProject(
                        active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                        'cursor-default select-none relative py-2 pl-3 pr-9'
                      )
                    }
                    value={null}
                  >
                    {({ selectedProject, active }) => (
                      <>
                        <div className="flex justify-between">
                          <span className={classNamesProject(active ? 'font-semibold' : 'font-normal', 'truncate')}>
                            All project
                          </span>
                        </div>
                      </>
                    )}
                  </Listbox.Option>

                  {projects && projects.map((el) => (
                    <Listbox.Option
                      key={el.id}
                      className={({ active }) =>
                        classNamesProject(
                          active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                          'cursor-default select-none relative py-2 pl-3 pr-9'
                        )
                      }
                      value={el}
                    >
                      {({ selectedProject, active }) => (
                        <>
                          <div className="flex justify-between">
                            <span className={classNamesProject(active ? 'font-semibold' : 'font-normal', 'truncate')}>
                              {el.name}
                            </span>
                          </div>
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </Listbox>
        </div>

        <div className='col-span-3 grid grid-cols-5 inline-flex'>
          <p className='flex w-full col-span-2'>Member :</p>
          <Listbox value={selectedMember} onChange={setSelectedMember}>
            <div className="relative mr-3 w-full col-span-3">
              <Listbox.Button className="relative w-full bg-white border border-ts-gray-500 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-0 focus:border-ts-gray-600 sm:text-sm">
                <span className="w-full inline-flex truncate">
                  <span className="truncate">{selectedMember ? selectedMember.name : 'All Member'}</span>
                </span>
                <span className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                  <img src="chevron-down.svg" alt="chevron-down" />
                </span>
              </Listbox.Button>
              <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                  <Listbox.Option
                    key={null}
                    className={({ active }) =>
                      classNamesMember(
                        active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                        'cursor-default select-none relative py-2 pl-3 pr-9'
                      )
                    }
                    value={null}
                  >
                    {({ selectedProject, active }) => (
                      <>
                        <div className="flex justify-between">
                          <span className={classNamesMember(active ? 'font-semibold' : 'font-normal', 'truncate')}>
                            All Member
                          </span>
                        </div>
                      </>
                    )}
                  </Listbox.Option>

                  {members && members.map((el) => (
                    <Listbox.Option
                      key={el.id}
                      className={({ active }) =>
                        classNamesMember(
                          active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                          'cursor-default select-none relative py-2 pl-3 pr-9'
                        )
                      }
                      value={el}
                    >
                      {({ selectedMember, active }) => (
                        <>
                          <div className="flex justify-between">
                            <span className={classNamesMember(active ? 'font-semibold' : 'font-normal', 'truncate')}>
                              {el.name}
                            </span>
                          </div>
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </Listbox>
        </div>
      </div>

      <div className='grid grid-cols-10 gap-4 px-10 justify-start'>
        <div className='col-span-3 grid grid-cols-5 inline-flex'>
          <p className='flex w-full col-span-2'>Since :</p>
          <div className='relative mr-3 w-full col-span-3'>
            <Flatpickr
              className='flex border border-ts-gray-500 rounded-md w-full mr-3'
              placeholder='Select Start Date'
              value={newStartDate}
              options={{
                enableTime: true,
                dateFormat: "d/m/Y H:i",
              }}
              onChange={([date]) => {
                setNewStartDate(date.toISOString());
              }}
            />
          </div>
        </div>
        <div className='col-span-3 grid grid-cols-5 inline-flex'>
          <p className='flex w-full col-span-2'>To :</p>
          <div className='relative mr-3 w-full col-span-3'>
            <Flatpickr
              className='flex border border-ts-gray-500 rounded-md w-full mr-3'
              placeholder='Select End Date'
              value={newEndDate}
              options={{
                enableTime: true,
                dateFormat: "d/m/Y H:i",
              }}
              onChange={([date]) => {
                setNewEndDate(date.toISOString());
              }}
            />
          </div>
        </div>
        <div className='col-span-2'>
          <button
            className="inline-flex justify-center py-2 px-4 space-x-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-ts-aquagreen-600 hover:bg-ts-aquagreen-700"
            onClick={(e) => {
              onHandlerExportReport()
              // console.log('generate report')
            }}
          >
            <DocumentDownloadIcon className='w-5 h-5' />
            <span>Generate Report</span>
          </button>

        </div>
      </div>

      <div className="bg-white divide-y divide-divider rounded-md shadow-md mt-5 py-3 w-full h-full">
        <div className="divide-y divide-divider w-full h-full overflow-x-scroll">

          <div className='inline-flex px-12 py-2 space-x-2'>
            <div className='w-44 items-center justify-center font-bold'>Lastest Update</div>
            <div className='w-28 items-center justify-start font-bold'>Group</div>
            <div className='w-32 items-center justify-start font-bold'>Project Name</div>
            <div className='w-72 items-center justify-start font-bold'>Task Name</div>
            <div className='w-32 items-center justify-start font-bold'>Subtask Name</div>
            <div className='w-40 items-center justify-start font-bold'>Start at</div>
            <div className='w-40 items-center justify-start font-bold'>Done at</div>
          </div>

          <div className='divide-y divide-divider px-4 w-full'>
            {historyLoading && (
              <div className='flex justify-center mt-2'>
                <div className='indicator'></div>
              </div>
            )}
            {historys.length === 0 && !historyLoading && (
              <>
                <div className='text-center text-sm italic text-ts-white-400 mt-2'>empty</div>
              </>
            )}

            {historys.map((history) => (
              <div className='inline-flex px-8 py-5 space-x-2 cursor-pointer' key={history.id}>
                <div className='w-44 items-center justify-start'>{isoToUtc(history.updatedDate)[0]} {isoToUtc(history.updatedDate)[1]}</div>
                <div className='w-28 items-center justify-start truncate'>{history.projectGroup.name}</div>
                <div className='w-32 items-center justify-start truncate'>{history.project.name}</div>
                <div className='w-72 items-center justify-start truncate'>{history.task.name}</div>
                <div className='w-32 items-center justify-start truncate'>{history.subtask.name}</div>
                <div className='w-40 items-center justify-start'>{isoToUtc(history.startTime)[0]} <span className='text-gray-500'>{isoToUtc(history.startTime)[1]}</span></div>
                <div className='w-40 items-center justify-start'>{isoToUtc(history.stopTime)[0]} <span className='text-gray-500'>{isoToUtc(history.stopTime)[1]}</span></div>
              </div>
            ))}
          </div>

        </div>
        <Pagination currentPage={pageNumber} totalPage={totalPage} onChangePage={onChangePage} />
      </div>
    </div>
  )
}

export default report
report.Layout = MainLayout
report.current = 'report'
report.title = 'Report'
report.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}