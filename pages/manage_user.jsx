import React from 'react'
import { useState, useEffect } from 'react'
import HeaderBox from '../components/HeaderBox'
import MainLayout from "../components/MainLayout"
import { getSession } from 'next-auth/client'
import ManageMemberDropdown from '../components/ManageMemberDropdown'

import {
  UserAddIcon,
  CogIcon,
  TrashIcon,
} from '@heroicons/react/outline'
import {
  ChevronDownIcon,
  CheckIcon,
} from '@heroicons/react/solid'

const mock_member = [
  { name: "jame Ji", email: "jame@entercorp.net", edit: false, manage_member: true, delete: true, role: "PM" },
  { name: "Angela Smith", email: "angela@entercorp.net", edit: false, manage_member: true, delete: true, role: "DEV" },
  { name: "Bella Smith", email: "bella@entercorp.net", edit: false, manage_member: true, delete: true, role: "UXUI" },
  { name: "Harry Ji", email: "harry@entercorp.net", edit: true, manage_member: false, delete: true, role: "BD" },
  { name: "Wendy Ji", email: "wendy@entercorp.net", edit: false, manage_member: true, delete: true, role: "DEV" },
  { name: "Sompong Ji", email: "sompong@entercorp.net", edit: false, manage_member: true, delete: true, role: "UXUI" },
  { name: "Somsri Ji", email: "somsri@entercorp.net", edit: true, manage_member: false, delete: true, role: "BD" },
]
const manage_user = () => {
  const [memberLoading, setMemberLoading] = useState(false)
  const [members, setMembers] = useState([])
  const [availableMember, setAvailableMember] = useState([])
  const [editMember, setEditMember] = useState(false);
  const [selectMember, setSelectMember] = useState({ name: 'Add member to project' })
  const [newRole, setNewRole] = useState("")
  const [newEdit, setNewEdit] = useState(false)
  const [newManage_Member, setNewManage_Member] = useState(false)
  const [newDelete, setNewDelete] = useState(false)

  useEffect(() => {
    loadMembers()
    loadAvailableMembers()
    console.log(selectMember)
  }, [])

  const loadMembers = async () => {
    setMemberLoading(true)
    setMembers(mock_member)
    setMemberLoading(false)
  }

  const loadAvailableMembers = async () => {
    const url = `/api/profile/`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setAvailableMember(data)
    } catch (error) {
      console.error(error)
    }
  }

  const onClickEditButton = (e, type, email) => {
    let tempmembers = members
    tempmembers.forEach(member => {
      if (member.email === email)
        switch (type) {
          case 'edit':
            member.edit = e.target.checked
            break
          case 'manage_member':
            member.manage_member = e.target.checked
            break
          case 'delete':
            member.delete = e.target.checked
            break
          case 'role':
            member.role = e.target.value
            break
        }
    })
    setMembers(tempmembers)
  }

  const handleRemoveMember = (e, email) => {
    setMembers(members.filter(member => member.email !== email))
  }

  const inviteButtonHandler = () => {
    let newmember = {
      "name": selectMember.name,
      "email": selectMember.email,
      "role": newRole,
      "edit": newEdit,
      "manage_member": newManage_Member,
      "delete": newDelete,
    }
    setMembers((prev) => [newmember, ...prev])
    setSelectMember({ name: 'Add member to project' })
    setNewRole("")
    setNewEdit(false)
    setNewManage_Member(false)
    setNewDelete(false)
  }

  return (
    <div className="max-w-8xl mx-auto px-4 sm:px-6 md:px-8 space-y-8">
      <HeaderBox title='Manage project member' content={'PEA - Inno Platform'} />

      { availableMember &&
        <div className="rounded-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 divide-y divide-divider">
          <div className='flex px-8 py-2'>
            <div className='col-span-1 flex items-center'>Add member to project</div>
          </div>
          <div className='px-8'>
            <div className='grid grid-cols-11 px-8 py-2 space-x-1'>
              <div className='col-span-1 flex items-center'>Member</div>
              <div className='col-span-4 flex items-center'>
                <ManageMemberDropdown availableMember={availableMember} selectMember={selectMember} setSelectMember={setSelectMember} />
              </div>
              <div className='col-span-1 flex items-center'></div>
              <div className='col-span-1 flex items-center'>Role</div>
              <div className='col-span-4 flex items-center'>
                <select className="inputbox rounded-md w-full" value={newRole} onChange={e => setNewRole(e.target.value)} >
                  <option value="" disabled selected>Role in this project</option>
                  <option className='hover:text-red-300' value="BD">BD </option>
                  <option value="CM">CM</option>
                  <option value="DevOps">DevOps</option>
                  <option value="PM">PM</option>
                  <option value="QA">QA</option>
                  <option value="SA">SA</option>
                  <option value="SWE">SWE</option>
                  <option value="SWT">SWT</option>
                  <option value="UXUI">UXUI</option>
                </select>
              </div>
            </div>

            <div className='grid grid-cols-11 px-8 py-2 space-x-1'>
              <div className='col-span-1 flex align-top  pt-1'>Permission</div>
              <div className='col-span-4 flex'>
                <div class="mt-2">
                  <div>
                    <label className="inline-flex items-center">
                      <input type="checkbox" checked={newEdit} onChange={e => setNewEdit(!newEdit)} className="form-checkbox" />
                      <span className="ml-4">Edit</span>
                    </label>
                  </div>
                  <div>
                    <label className="inline-flex items-center">
                      <input type="checkbox" checked={newManage_Member} onChange={e => setNewManage_Member(!newManage_Member)} className="form-checkbox" />
                      <span className="ml-4">Manage member</span>
                    </label>
                  </div>
                  <div>
                    <label className="inline-flex items-center">
                      <input type="checkbox" checked={newDelete} onChange={e => setNewDelete(!newDelete)} className="form-checkbox" />
                      <span className="ml-4">Delete</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div className='col-span-4 flex items-center justify-end font-bold'>
              {selectMember.name === 'Add member to project' ?
                <button disabled className='button-default px-6 bg-ts-aquagreen-600 hover:bg-ts-aquagreen-700 disabled:opacity-50'
                  onClick={(e) => { inviteButtonHandler() }}
                >
                  <p>Invite</p>
                </button>
                :
                <button className='button-default px-6 bg-ts-aquagreen-600 hover:bg-ts-aquagreen-700d'
                  onClick={(e) => { inviteButtonHandler() }}
                >
                  <p>Invite</p>
                </button>
              }
            </div>

          </div>
        </div>
      }


      <div className='grid px-5 space-x-2 grid-cols-8'>
        <div className='col-span-4 flex items-center font-bold'>Member</div>
        <div className='col-span-4 flex items-center justify-end font-bold'>
          <button className='button-default' onClick={() => {
            setEditMember(!editMember)
          }

          }>
            {editMember ?
              <>
                <CheckIcon className='w-5 h-5' />
                <p>Save changes</p>
              </> :
              <>
                <CogIcon className='w-5 h-5' />
                <p>Edit member</p>
              </>
            }

          </button>
        </div>
      </div>

      {members &&
        <div className="rounded-lg bg-white shadow sm:rounded-lg border-ts-blue-900 py-2 divide-y divide-divider">
          <div className='grid grid-cols-12 px-8 py-2 space-x-1'>
            <div className='col-span-5 flex px-4 items-center font-bold'>Name</div>
            <div className='col-span-1 flex items-center font-bold'>Edit</div>
            <div className='col-span-2 flex items-center font-bold justify-center'>Manage Member</div>
            <div className='col-span-2 flex items-center font-bold justify-center'>Delete</div>
            <div className='col-span-2 flex items-center font-bold justify-start'>Role</div>
          </div>

          {members.map((member) => (
            <div className='my-2' key={member.id}>
              <div className='grid grid-cols-12 flex px-4 py-4 rounded-t-lg'>
                <div className='col-span-2 flex items-center ml-7'>
                  <p>{member.name}</p>
                </div>
                <div className='col-span-3 flex items-center space-x-2'>
                  <span className="text-gray-400">{`${member.email}`}</span>
                </div>

                {!editMember &&
                  <div className='col-span-7 items-center space-x-3'>
                    <div className='grid grid-cols-8 px-4 py-4 rounded-t-lg gap-10'>
                      <div className="col-span-2 items-center">
                        <input type={member.edit ? "checkbox" : "hidden"} disabled checked={member.edit} />
                      </div>
                      <div className="col-span-2 items-center justify-center">
                        <input type={member.manage_member ? "checkbox" : "hidden"} disabled checked={member.manage_member} />
                      </div>
                      <div className="col-span-1 items-center justify-end ml-5">
                        <input type={member.delete ? "checkbox" : "hidden"} disabled checked={member.delete} />
                      </div>
                      <div className="col-span-2 flex items-center ml-10">
                        <p className="col-start-3">{member.role}</p>
                      </div>
                    </div>
                  </div>
                }
                {editMember &&
                  <div className='col-span-7 items-center space-x-3'>
                    <div className='grid grid-cols-8 px-4 py-4 rounded-t-lg gap-10'>
                      <div className="col-span-2 items-center">
                        <input type="checkbox" className="cursor-pointer" onClick={(e) => onClickEditButton(e, 'edit', member.email)} defaultChecked={member.edit} />
                      </div>
                      <div className="col-span-2 items-center justify-center">
                        <input type="checkbox" className="cursor-pointer" onClick={(e) => onClickEditButton(e, 'manage_member', member.email)} defaultChecked={member.manage_member} />
                      </div>
                      <div className="col-span-1 items-center justify-end ml-5">
                        <input type="checkbox" className="cursor-pointer" onClick={(e) => onClickEditButton(e, 'delete', member.email)} defaultChecked={member.delete} />
                      </div>
                      <div className="col-span-2 flex items-center ml-8">
                        <select className="inputbox rounded-md" defaultValue={member.role}
                          onChange={(e) => onClickEditButton(e, 'role', member.email)}
                        >
                          <option value="BD">BD </option>
                          <option value="CM">CM</option>
                          <option value="DevOps">DevOps</option>
                          <option value="PM">PM</option>
                          <option value="QA">QA</option>
                          <option value="SA">SA</option>
                          <option value="SWE">SWE</option>
                          <option value="SWT">SWT</option>
                          <option value="UXUI">UXUI</option>
                        </select>
                      </div>
                      <div className="col-span-1 items-center justify-end">
                        <button className='button-default bg-red-500'
                          onClick={(e) => { handleRemoveMember(e, member.email) }}
                        >
                          <TrashIcon className='w-5 h-5' />
                        </button>
                      </div>
                    </div>
                  </div>
                }
              </div>

             



            </div>

          ))}
        </div>
      }
    </div >

  )
}

export default manage_user
manage_user.Layout = MainLayout;
manage_user.current = 'project'
manage_user.title = 'Manage user'
manage_user.getInitialProps = async (context) => {
  const { req, res } = context
  const session = await getSession({ req })
  if (!session) {
    res.writeHead(302, {
      Location: '/login'
    })
    res.end()
    return
  }
  return {
    user: session
  }
}
