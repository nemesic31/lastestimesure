export const getToken = (request) => {
  const secureCookieName = '__Secure-next-auth.session-token'
  const insecureCookieName = 'next-auth.session-token'
  const token = request.cookies[secureCookieName] || request.cookies[insecureCookieName];
  return token;
}