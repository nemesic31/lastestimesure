const ContentBox = ({content}) => {
  return (
    <div className="rounded-lg bg-white py-3 px-8 shadow sm:rounded-lg border-ts-blue-900">
      <div>
        {content}
      </div>
    </div>
  )
}

export default ContentBox
