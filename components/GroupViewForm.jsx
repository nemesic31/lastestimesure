import { useState, Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon } from '@heroicons/react/outline'
import { PlusCircleIcon } from '@heroicons/react/outline'
import dynamic from 'next/dynamic';
import '@uiw/react-markdown-preview/markdown.css';
const MarkdownPreview = dynamic(
  () => import("@uiw/react-markdown-preview").then((mod) => mod.default),
  { ssr: false }
);

const GroupViewForm = ({ open, onClose, dataGroup }) => {
  const isoToUtc = (isoString) => {
    var utcDate = new Date(isoString).toLocaleDateString('en-GB')
    var utcTime = new Date(isoString).toLocaleTimeString('en-GB')
    return [utcDate, utcTime]
  }
  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as='div' className='fixed inset-0 overflow-hidden z-10' onClose={onClose}>
        <div className='absolute inset-0 overflow-hidden'>
          <Dialog.Overlay className='absolute inset-0' />

          <div className='fixed inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16'>
            <Transition.Child
              as={Fragment}
              enter='transform transition ease-in-out duration-500 sm:duration-700'
              enterFrom='translate-x-full'
              enterTo='translate-x-0'
              leave='transform transition ease-in-out duration-500 sm:duration-700'
              leaveFrom='translate-x-0'
              leaveTo='translate-x-full'
            >
              <div className='w-screen max-w-2xl'>
                <div className='h-full flex flex-col bg-white shadow-xl overflow-y-scroll'>
                  <div className='flex-1'>
                    {/* Header */}
                    <div className='px-4 py-6 sm:px-6 bg-ts-aquagreen-100'>
                      <div className='flex items-start justify-between space-x-3'>
                        <div className='space-y-1'>
                          <Dialog.Title className='text-lg font-medium text-gray-900 flex space-x-2 items-center'>
                            <p>Group</p>
                          </Dialog.Title>
                        </div>
                        <div className='h-7 flex items-center'>
                          <button
                            type='button'
                            className='text-gray-400 hover:text-gray-500'
                            onClick={onClose}
                          >
                            <span className='sr-only'>Close panel</span>
                            <XIcon className='h-6 w-6' aria-hidden='true' />
                          </button>
                        </div>
                      </div>
                    </div>

                    {/* Divider container */}
                    <div className='py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200'>
                      {/* name */}
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Group name
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>{dataGroup && dataGroup.name}</p>
                        </div>
                      </div>

                      {/* description */}
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-description'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Description
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <MarkdownPreview source={dataGroup && dataGroup.description} />
                        </div>
                      </div>

                      {/* Create at */}
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Created at
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>{dataGroup && isoToUtc(dataGroup.createdDate)[0] + ' ' + isoToUtc(dataGroup.createdDate)[1]}</p>
                        </div>
                      </div>

                      {/* Create by */}
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Created by
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>{dataGroup && dataGroup.profile.name}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

export default GroupViewForm
