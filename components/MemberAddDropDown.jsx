import { Fragment, useState, useEffect } from 'react'
import { Listbox, Transition } from '@headlessui/react'
import { classNames } from '../utilities/css-util'

import {
  ChevronDownIcon,
  CheckIcon
} from '@heroicons/react/solid'
import {
  XCircleIcon,
} from '@heroicons/react/outline'

export default function MemberAddDropDown({ availableMember, member, setMember }) {
  const people = availableMember.map(member => ({ name: member.name, id: member.id }))
  const [selectedPersons, setSelectedPersons] = useState([]);

  function isSelected(value) {
    return selectedPersons.find((el) => el === value) ? true : false;
  }

  function handleSelection(person) {
    const selectedResult = selectedPersons.filter(
      (selected) => selected === person
    );
    if (selectedResult.length) {
      removePerson(person);
    } else {

      setSelectedPersons((currents) => [...currents, person]);
      const newMember = selectedPersons
      newMember.push(person);
      setMember(newMember);
    }
  }

  function removePerson(person) {
    const removedSelection = selectedPersons.filter(
      (selected) => selected !== person
    );
    setSelectedPersons(removedSelection);
  }

  return (
    <Listbox
      as="div"
      className="space-y-1"
      value={selectedPersons}
      onChange={handleSelection}
    >
      {({ open }) =>
        <>
          <Listbox.Label className="block text-sm leading-5 font-medium text-gray-700">
            Assigned to
          </Listbox.Label>
          <div className="relative">
            <span className="inline-block w-full rounded-md shadow-sm">
              <Listbox.Button className="cursor-default relative w-full rounded-md border border-gray-300 bg-white pl-3 pr-10 py-2 text-left focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                {!selectedPersons.length && "One or more persons"}
                {selectedPersons.map((person) => (
                  <div
                    key={person.id}
                    className="inline-flex items-center px-1 mr-1 mt-1 rounded text-white bg-gray-400"
                  >
                    {person.name}
                    <div
                      className="ml-1 bg-gray-100 rounded-full cursor-pointer"
                      onClick={() => removePerson(person)}
                    >
                      <XCircleIcon className='h-4 w-4 bg-gray-400' />
                    </div>
                  </div>
                ))}
                <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                  <ChevronDownIcon className='h-5 w-5 text-gray-400' />
                </span>
              </Listbox.Button>
            </span>

            <Transition
              as={Fragment}
              show={open}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
              className="absolute mt-1 w-full rounded-md bg-white shadow-lg"
            >
              <Listbox.Options
                static
                className="max-h-60 rounded-md py-1 text-base leading-6 shadow-xs overflow-auto focus:outline-none sm:text-sm sm:leading-5"
              >
                {people.map((person) => {
                  const selected = isSelected(person);
                  return (
                    <Listbox.Option key={person.id} value={person}>
                      {({ active }) => (
                        <div
                          className={`${active
                            ? "text-white bg-blue-600"
                            : "text-gray-900"
                            } cursor-default select-none relative py-2 pl-8 pr-4`}
                        >
                          <span
                            className={`${selected ? "font-semibold" : "font-normal"
                              } block truncate`}
                          >
                            {person.name}
                          </span>
                          {selected && (
                            <span
                              className={`${active ? "text-white" : "text-blue-600"
                                } absolute inset-y-0 left-0 flex items-center pl-1.5`}
                            >
                              <CheckIcon className='h-5 w-5' />
                            </span>
                          )}
                        </div>
                      )}
                    </Listbox.Option>
                  );
                })}
              </Listbox.Options>
            </Transition>
          </div>
        </>
      }
    </Listbox>
  )
}