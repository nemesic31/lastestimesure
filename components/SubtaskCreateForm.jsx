import { useState, Fragment, useEffect } from 'react'
import { Dialog, Transition, Listbox, Switch } from '@headlessui/react'
import { useForm } from "react-hook-form";
import { getSession, useSession } from 'next-auth/client'
import ReactMde, { SaveImageHandler } from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";
import { v4 } from 'uuid';
import MemberDropDown from './MemberDropDown'
import AddMemberForAssignee from './AddMemberForAssignee'


function loadSuggestions(text) {
  return new Promise((accept, reject) => {
    setTimeout(() => {
      const suggestions = [
        {
          preview: "Andre",
          value: "@andre"
        },
        {
          preview: "Angela",
          value: "@angela"
        },
        {
          preview: "David",
          value: "@david"
        },
        {
          preview: "Louise",
          value: "@louise"
        }
      ].filter(i => i.preview.toLowerCase().includes(text.toLowerCase()));
      accept(suggestions);
    }, 250);
  });
}

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});

function classNamesToggle(...classes) {
  return classes.filter(Boolean).join(' ')
}



const subtaskCreateForm = ({ taskdata, open, onClose,availableMember,onSave }) => {
  const [description, setDescription] = useState("");
  const [selectedTab, setSelectedTab] = useState("write");
  const date = new Date();
  const [subtaskDate, setSubtaskDate] = useState()
  const [selectedRole, setSelectedRole] = useState()
  const [projectManager, setProjectManager] = useState({ name: 'Assign task to...' });
  const [enabledToggle, setEnabledToggle] = useState(false)


  const {
    register,
    handleSubmit,
    watch,
    reset,
  } = useForm();

  const watchAllFields = watch();


  const handleSubmitSubtask = async (value) => {
    const { subtaskName, hourEstimated, minuteEstimated, hourSpent, minuteSpent } = value
    const result = Number(hourEstimated)*60+Number(minuteEstimated)
    const listProfile = projectManager.map(el => ({
      profile: {
        id: el.id
      }
    }))
    const dataPack =
            {
              name: subtaskName,
              description: description,
              estimatedTime: result,
              active: true,
              task: {
                id: taskdata.taskId
              },
              role: {
                id: "abe40b26-8960-428d-9eba-fd9141c28f69"
              },
              subtaskAssignees: listProfile
            }
    onSave(dataPack)
    setProjectManager(({ name: 'Assign task to...' }))
    reset()
    setSelectedRole()
    setEnabledToggle(false)
    setDescription('')
    onClose()
  }

  const statusDisable = () => {

    if ((watchAllFields.hourEstimated || watchAllFields.minuteEstimated) && projectManager && selectedRole && watchAllFields.subtaskName && !enabledToggle) {
      return false
    }

    if ((watchAllFields.hourEstimated || watchAllFields.minuteEstimated) && projectManager && selectedRole && watchAllFields.subtaskName && enabledToggle && (watchAllFields.hourSpent || watchAllFields.minuteSpent)) {
      return false
    }

    return true

  }

  const handleKeyPressTimeEstimate = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }

  }

  const handleKeyPressTimeSpent = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  }

  useEffect(() => {
    setSubtaskDate(date.toLocaleString())
  }, [date])


  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as='div' className='fixed inset-0 overflow-hidden z-10' onClose={onClose}>
        <div className='absolute inset-0 overflow-hidden'>
          <Dialog.Overlay className='absolute inset-0' />

          <div className='fixed inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16'>
            <Transition.Child
              as={Fragment}
              enter='transform transition ease-in-out duration-500 sm:duration-700'
              enterFrom='translate-x-full'
              enterTo='translate-x-0'
              leave='transform transition ease-in-out duration-500 sm:duration-700'
              leaveFrom='translate-x-0'
              leaveTo='translate-x-full'
            >
              <div className='w-screen max-w-2xl'>
                <form className='h-full flex flex-col bg-white shadow-xl overflow-y-scroll' onSubmit={handleSubmit(handleSubmitSubtask)}>
                  <div className='flex-1'>
                    {/* Header */}
                    <div className='px-4 py-6 sm:px-6 bg-ts-aquagreen-200'>
                      <div className='flex items-start justify-between space-x-3'>
                        <div className='space-y-1'>
                          <Dialog.Title className='text-lg font-medium text-gray-900 flex space-x-2 items-center'>
                            <img src="addSubtask-icon.svg" alt="addSubtask-icon" />
                            <p className="ml-6 text-lg font-normal">Create  Subtask</p>
                          </Dialog.Title>
                        </div>
                        <div className='h-7 flex items-center'>
                          <button
                            type='button'
                            onClick={onClose}
                            className="ring-0 border-transparent"
                          >
                            <img src="close-icon.svg" alt="close-icon" />
                          </button>
                        </div>
                      </div>
                    </div>

                    {/* Divider container */}
                    <div className="bg-ts-yellow-500 text-white w-max px-4 py-1 mt-6 mx-6 rounded-md">
                      TODO
                    </div>
                    <div className='py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200'>
                      {/* body */}
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 '
                          >
                            Create at
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center '>
                          <p>
                            {subtaskDate}
                          </p>
                        </div>
                      </div>
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Project
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>
                          {taskdata.groupName} - {taskdata.projectName}
                          </p>
                        </div>
                      </div>
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Task
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>
                          {taskdata.taskName}
                          </p>
                        </div>
                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Subtask name
                          </label>
                        </div>

                        <div className='sm:col-span-2 flex items-center'>
                          <input
                            type='text' className='inputbox w-full'
                            name="subtaskName"
                            id="subtaskName"
                            {...register("subtaskName", { required: true })}
                          />
                        </div>

                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-description'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Description
                          </label>
                        </div>
                        <div className='sm:col-span-2'>
                          <ReactMde
                            value={description}
                            onChange={setDescription}
                            selectedTab={selectedTab}
                            onTabChange={setSelectedTab}
                            generateMarkdownPreview={markdown =>
                              Promise.resolve(converter.makeHtml(markdown))
                            }
                            loadSuggestions={loadSuggestions}
                            childProps={{
                              writeButton: {
                                tabIndex: -1
                              }
                            }}
                          />
                        </div>
                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Role
                          </label>
                        </div>
                        <div className='sm:col-span-2'>
                          <select class="inputbox rounded-md w-full mt-1"
                              onChange={e => setSelectedRole(e.target.value)}
                              required
                          >
                                    <option value="" disabled selected>Your role in this project</option>
                                    <option className='hover:text-red-300' value="BD">BD </option>
                                    <option value="CM">CM</option>
                                    <option value="DevOps">DevOps</option>
                                    <option value="PM">PM</option>
                                    <option value="QA">QA</option>
                                    <option value="SA">SA</option>
                                    <option value="SWE">SWE</option>
                                    <option value="SWT">SWT</option>
                                    <option value="UXUI">UXUI</option>
                                    </select>
                        </div>
                      </div>

                      <div className='px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Assignee
                          </label>
                        </div>
                        <div className='sm:col-span-2 z-20'>
                      <AddMemberForAssignee availableMember={availableMember} member={projectManager} setMember={setProjectManager} />
                        </div>
                      <div>
                      </div>
                      </div>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                          >
                            Estimated time
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <input
                            type="number"
                            name="hourEstimated"
                            id="hourEstimated"
                            onKeyPress={(event) => handleKeyPressTimeEstimate(event)}
                            className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                            {...register("hourEstimated")}
                            min="0"
                          />

                          <p className="mx-2">
                            hour(s)
                          </p>
                          <input
                            type="number"
                            name="minuteEstimated"
                            onKeyPress={(event) => handleKeyPressTimeEstimate(event)}
                            id="minuteEstimated"
                            className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                            {...register("minuteEstimated")}
                            min="0"
                          />
                          <p className=" mx-2">
                            minute(s)
                          </p>
                        </div>
                      </div>

                      <div>
                        <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                            >
                              Past task
                            </label>
                            <p className="text-ts-gray-600 text-xs">To specify the working hours backwards.
                              This task will be recorded as done.</p>
                          </div>
                          <div className='flex sm:col-span-2 items-center'>
                            <Switch
                              checked={enabledToggle}
                              onChange={setEnabledToggle}
                              className={classNamesToggle(
                                enabledToggle ? 'bg-ts-green-400' : 'bg-gray-200',
                                'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                              )}
                            >
                              <span className="sr-only">Use setting</span>
                              <span
                                aria-hidden="true"
                                className={classNamesToggle(
                                  enabledToggle ? 'translate-x-5' : 'translate-x-0',
                                  'pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                )}
                              />
                            </Switch>
                          </div>
                        </div>

                      {
                        enabledToggle && 
                        <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          
                          <div>
                            <label
                              htmlFor='group-name'
                              className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Time spent
                            </label>
                          </div>

                          <div className='sm:col-span-2 flex items-center'>
                            <input
                              type="number"
                              name="hourSpent"
                              id="hourSpent"
                              onKeyPress={(event) => handleKeyPressTimeSpent(event)}
                              min="0"
                              className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                              {...register("hourSpent")}
                            />

                            <p className="mx-2">
                              hour(s)
                            </p>
                            <input
                              type="number"
                              name="minuteSpent"
                              id="minuteSpent"
                              onKeyPress={(event) => handleKeyPressTimeSpent(event)}
                              min="0"
                              className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                              {...register("minuteSpent")}
                            />
                            <p className=" mx-2">
                              minute(s)
                            </p>
                          </div>
                        </div>
                      }
                      </div>


                    </div>
                  </div>
                  {/* Action buttons */}
                  <div className='flex-shrink-0 px-4 border-t border-gray-200 py-5 sm:px-6'>

                    <div className='space-x-3 flex justify-end'>

                      <button
                        type='button'
                        className='bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                        onClick={onClose}
                      >
                        Cancel
                      </button>

                      <button
                        type='submit'
                        disabled={statusDisable()}
                        className={`ml-4 text-white text-base ${statusDisable() === false ? 'bg-ts-aquagreen-600' : 'bg-ts-gray-600'} 
                        px-8 py-1 rounded-md`}
                      >
                        Create
                      </button>

                    </div>

                  </div>
                </form>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

export default subtaskCreateForm
