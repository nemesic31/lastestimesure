
import { useState, Fragment } from 'react'
import { Dialog, Transition, Listbox } from '@headlessui/react'
import { XIcon } from '@heroicons/react/outline'
import { LinkIcon, PlusSmIcon, QuestionMarkCircleIcon } from '@heroicons/react/solid'
import { PlusCircleIcon } from '@heroicons/react/outline'
import MemberDropDown from './MemberDropDown'
import GroupDropDown from './GroupDropDown'
import MemberAddDropDown from './MemberAddDropDown'
import { useRouter } from 'next/router'
import {
    UserAddIcon
} from '@heroicons/react/solid'




import { CheckIcon, SelectorIcon } from '@heroicons/react/solid'

import ReactMde from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";

function loadSuggestions(text) {
    return new Promise((accept, reject) => {
        setTimeout(() => {
            const suggestions = [
                {
                    preview: "Andre",
                    value: "@andre"
                },
                {
                    preview: "Angela",
                    value: "@angela"
                },
                {
                    preview: "David",
                    value: "@david"
                },
                {
                    preview: "Louise",
                    value: "@louise"
                }
            ].filter(i => i.preview.toLowerCase().includes(text.toLowerCase()));
            accept(suggestions);
        }, 250);
    });
}

const converter = new Showdown.Converter({
    tables: true,
    simplifiedAutoLink: true,
    strikethrough: true,
    tasklists: true
});

const ProjectCreateForm = ({ open,onClose,dataProject }) => {
    const router = useRouter()
    const {pathname} = router
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [selectedTab, setSelectedTab] = useState("write");
    const [projectManager, setProjectManager] = useState({ name: 'Assign task to...' });
    const [groupSelected, setGroupSelected] = useState({ name: 'Select Group ...' });

    const [member, setMember] = useState('');

    


    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as='div' className='fixed inset-0 overflow-hidden z-10' onClose={onClose}>
                <div className='absolute inset-0 overflow-hidden'>
                    <Dialog.Overlay className='absolute inset-0' />

                    <div className='fixed inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16'>
                        <Transition.Child
                            as={Fragment}
                            enter='transform transition ease-in-out duration-500 sm:duration-700'
                            enterFrom='translate-x-full'
                            enterTo='translate-x-0'
                            leave='transform transition ease-in-out duration-500 sm:duration-700'
                            leaveFrom='translate-x-0'
                            leaveTo='translate-x-full'
                        >
                            <div className='w-screen max-w-2xl'>
                                <form className='h-full flex flex-col bg-white shadow-xl overflow-y-scroll' >
                                    <div className='flex-1'>
                                        {/* Header */}
                                        <div className='px-4 py-6 sm:px-6 bg-ts-aquagreen-100'>
                                            <div className='flex items-start justify-between space-x-3'>
                                                <div className='space-y-1'>
                                                    <Dialog.Title className='text-lg font-medium text-gray-900 flex space-x-2 items-center'>
                                                        <p>Project</p>
                                                    </Dialog.Title>
                                                </div>
                                                <div className='h-7 flex items-center'>
                                                    <button
                                                        type='button'
                                                        className='text-gray-400 hover:text-gray-500'
                                                        onClick={onClose}
                                                    >
                                                        <span className='sr-only'>Close panel</span>
                                                        <XIcon className='h-6 w-6' aria-hidden='true' />
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        {/* Divider container */}
                                        <div className='py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200'>
                                            {/* name */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Project name
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2 flex items-center'>
                                                    <p>{dataProject && dataProject.name}</p>
                                                </div>
                                            </div>

                                            {/* description */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-description'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Description
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2 flex items-center'>
                                                    <p>{dataProject && dataProject.description}</p>
                                                </div>
                                            </div>
                                            {/* project manager */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Group
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2 flex items-center'>
                                                    <p>{dataProject && dataProject.projectGroup.name}</p>
                                                </div>
                                            </div>

                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Project manager
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2 flex items-center'>
                                                    <p>{dataProject && dataProject.profile.name}</p>
                                                </div>
                                            </div>

                                            {/* member */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Member
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2 w-full'>
                                                    {dataProject && dataProject.projectMembers.map((member)=>{
                                                        return(
                                                    <div key={member.id} className="flex w-max text-white bg-gray-600 p-2 rounded-lg mb-1">
                                                        <p>{member.profile.name}</p>
                                                    </div>
                                                    )})}
                                                </div>
                                            </div>

                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Template
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2 z-0'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
                        </Transition.Child>
                    </div>

                </div>
            </Dialog>
        </Transition.Root>
    )
}

export default ProjectCreateForm
