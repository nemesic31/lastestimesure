import { useState, Fragment, useEffect } from 'react'
import { Dialog, Transition, Listbox, Switch } from '@headlessui/react'
import ReactMde from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";
import dynamic from 'next/dynamic';
import '@uiw/react-markdown-preview/markdown.css';
const MarkdownPreview = dynamic(
  () => import("@uiw/react-markdown-preview").then((mod) => mod.default),
  { ssr: false }
);

function loadSuggestions(text) {
  return new Promise((accept, reject) => {
    setTimeout(() => {
      const suggestions = [
        {
          preview: "Andre",
          value: "@andre"
        },
        {
          preview: "Angela",
          value: "@angela"
        },
        {
          preview: "David",
          value: "@david"
        },
        {
          preview: "Louise",
          value: "@louise"
        }
      ].filter(i => i.preview.toLowerCase().includes(text.toLowerCase()));
      accept(suggestions);
    }, 250);
  });
}


const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});



const role = [
  { name: 'BD', username: 'Business Developer' },
  { name: 'PM', username: 'Project Manager' },
  { name: 'DEV', username: 'Developer' }
]

const people = [
  {
    id: 1,
    name: 'Assign yourself',
    avatar: ''
  },
  {
    id: 2,
    name: 'Arlene Mccoy',
    avatar:
      'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  },
  {
    id: 3,
    name: 'Devon Webb',
    avatar:
      'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80',
  },
  {
    id: 4,
    name: 'Wade Cooper',
    avatar:
      'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  },
]

function classNamesAssignee(...classes) {
  return classes.filter(Boolean).join(' ')
}

function classNamesRole(...classes) {
  return classes.filter(Boolean).join(' ')
}


const subtaskViewForm = ({ taskdata,dataCard, open, onClose, onSaveComment, editData, editComment }) => {

  const [editSubtaskName, setEditSubtaskName] = useState(false)
  const [newSubtaskName, setNewSubtaskName] = useState('')
  const [editDescription, setEditDescription] = useState(false)
  const [newDescription, setNewDescription] = useState('')
  const [editRole, setEditRole] = useState(false)
  const [newRole, setNewRole] = useState()
  const [editAssignee, setEditAssignee] = useState(false)
  const [newAssignee, setNewAssignee] = useState()
  const [editEstimate, setEditEstimate] = useState(false)
  const [newEstimate, setNewEstimate] = useState({ hour: '', minute: '' })
  const [editSpent, setEditSpent] = useState(false)
  const [newSpent, setNewSpent] = useState({ hour: '', minute: '' })
  const [selectedTabDescription, setSelectedTabDescription] = useState("write");
  const [selectedTabComment, setSelectedTabComment] = useState("write");
  const [comments, setComments] = useState("")
  const [newCommentsBoard, setNewCommentsBoard] = useState()
  const [newDataCard, setNewDataCard] = useState()
  const [statusAddDescription, setStatusAddDescription] = useState(false)

  const [statusEditComments, setstatusEditComments] = useState({ status: false, index: '' })
  const [newEditComments, setnewEditComments] = useState('')
  const [selectedTabEditComments, setSelectedTabEditComments] = useState("write");


  const handleSubmitCommentSubtask = () => {
    if (dataCard && comments) {
      onSaveComment(dataCard, comments)
      getSubtaskById()
    }
  }
  const getSubtaskById = async () => {
    try {
      if (dataCard) {
        const { id, items } = dataCard
        const response = await fetch(`api/subtask/viewsubtaskbyid?id=${id}&idsubtask=${items.id}`)
        const data = await response.json()
        if (data.comment) {
          setNewCommentsBoard(data.comment)
          setComments("")
        }
        if (data) {
          setNewDataCard(data)
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getSubtaskById()
  }, [dataCard])

  const submitEditComment = async (idComment) => {
    const { id, items } = dataCard
    if (newEditComments) {
      const newValue = {
        idCard: id,
        idSubtask: items.id,
        newValue: newEditComments,
        idComment: idComment
      }
      editComment(newValue)
      getSubtaskById()
      setnewEditComments('')
      setstatusEditComments({ status: false, index: '' })
    }
    setnewEditComments('')
    setstatusEditComments({ status: false, index: '' })
  }


  const submitEdit = async (value) => {
    if (newSubtaskName || newDescription || newRole || newAssignee || newEstimate || newSpent) {
      const { id, items } = dataCard
      const newdata = {
        idCard: id,
        idSubtask: items.id,
        idData: value.id,
        newValue: value.id === 'name' ? newSubtaskName
          : value.id === 'role' ? newRole
            : value.id === 'description' ? newDescription
              : value.id === 'assign' ? newAssignee
                : value.id === 'estimate' ? newEstimate
                  : newSpent,
      }
      editData(newdata)
      getSubtaskById()
      if (value.id === 'name') {
        setNewSubtaskName('')
        setEditSubtaskName(false)
      } else if (value.id === 'description') {
        setNewDescription('')
        setEditDescription(false)
        setStatusAddDescription(false)
      } else if (value.id === 'role') {
        setNewRole()
        setEditRole(false)
      } else if (value.id === 'assign') {
        setNewAssignee()
        setEditAssignee(false)
      } else if (value.id === 'estimate') {
        setNewEstimate({ hour: '', minute: '' })
        setEditEstimate(false)
      } else if (value.id === 'spent') {
        setNewSpent({ hour: '', minute: '' })
        setEditSpent(false)
      }
    } else {
      if (value.id === 'name') {
        setNewSubtaskName('')
        setEditSubtaskName(false)
      } else if (value.id === 'description') {
        setNewDescription('')
        setEditDescription(false)
        setStatusAddDescription(false)
      } else if (value.id === 'role') {
        setNewRole()
        setEditRole(false)
      } else if (value.id === 'assign') {
        setNewAssignee()
        setEditAssignee(false)
      } else if (value.id === 'estimate') {
        setNewEstimate({ hour: '', minute: '' })
        setEditEstimate(false)
      } else if (value.id === 'spent') {
        setNewSpent({ hour: '', minute: '' })
        setEditSpent(false)
      }
    }
  }

  const handleKeyPressTimeEstimate = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }

  }

  const handleKeyPressTimeSpent = (event) => {
    if (event.which !== 8 && isNaN(String.fromCharCode(event.which))) {
      event.preventDefault();
    }
  }




  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as='div' className='fixed inset-0 overflow-hidden z-10' onClose={onClose}>
        <div className='absolute inset-0 overflow-hidden'>
          <Dialog.Overlay className='absolute inset-0' />

          <div className='fixed inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16'>
            <Transition.Child
              as={Fragment}
              enter='transform transition ease-in-out duration-500 sm:duration-700'
              enterFrom='translate-x-full'
              enterTo='translate-x-0'
              leave='transform transition ease-in-out duration-500 sm:duration-700'
              leaveFrom='translate-x-0'
              leaveTo='translate-x-full'
            >
              <div className='w-screen max-w-2xl'>
                <form className='h-full flex flex-col bg-white shadow-xl overflow-y-scroll'>
                  <div className='flex-1'>
                    <div className='px-4 py-6 sm:px-6 bg-ts-aquagreen-200'>
                      <div className='flex items-start justify-between space-x-3'>
                        <div className='space-y-1'>
                          <Dialog.Title className='text-lg font-medium text-gray-900 flex space-x-2 items-center'>
                            <img src="addSubtask-icon.svg" alt="addSubtask-icon" />
                            <p className="ml-6 text-lg font-normal">Subtask</p>
                          </Dialog.Title>
                        </div>
                        <div className='h-7 flex items-center'>
                          <button
                            type='button'
                            onClick={onClose}
                            className="ring-0 border-transparent"
                          >
                            <img src="close-icon.svg" alt="close-icon" />
                          </button>
                        </div>
                      </div>
                    </div>

                    <div
                      className={`${dataCard && dataCard.key === 'doing' ? 'bg-ts-green-400'
                        : dataCard && dataCard.title === 'done' ? 'bg-ts-blue-500'
                          : dataCard && dataCard.title === 'close' ? 'bg-ts-red-600'
                            : 'bg-ts-yellow-500'} text-white w-max px-4 py-1 mt-6 mx-6 rounded-md`}
                    >
                      {dataCard && dataCard.key.toUpperCase()}
                    </div>
                    <div className='py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200'>

                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 '
                          >
                            Create at
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center '>
                          <p>
                            {dataCard && (new Date(`${dataCard.data.createdDate}`).toLocaleDateString())}
                          </p>
                        </div>
                      </div>
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Project
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>
                          {taskdata && taskdata.groupName} - {taskdata && taskdata.projectName}
                          </p>
                        </div>
                      </div>
                      <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                        <div>
                          <label
                            htmlFor='group-name'
                            className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                          >
                            Task
                          </label>
                        </div>
                        <div className='sm:col-span-2 flex items-center'>
                          <p>
                          {taskdata && taskdata.taskName}
                          </p>
                        </div>
                      </div>
                      {dataCard &&
                        <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5 flex items-center '>
                          <div>
                            <label
                              htmlFor='group-name'
                              className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Subtask name
                            </label>
                          </div>
                          <div className='sm:col-span-2 flex justify-between pr-12'>
                            {editSubtaskName ?
                              <div className="flex justify-between w-full">
                                <input className="border border-ts-gray-500 rounded-md" value={newSubtaskName} onChange={(e) => setNewSubtaskName(e.target.value)} />
                                <div className="flex">
                                  <button type="button" className="m-1" onClick={() => setEditSubtaskName(false)}>
                                    <p className="underline text-ts-gray-600 text-base">
                                      Cancel
                                    </p>
                                  </button>
                                  <button type="button" className="m-1" onClick={() => submitEdit({ id: 'name' })}>
                                    <p className="underline text-ts-gray-600 text-base">
                                      save
                                    </p>
                                  </button>
                                </div>
                              </div>
                              :
                              <div className="flex justify-between w-full">
                                <p>{dataCard && dataCard.data.name}</p>
                                <button type="button" onClick={() => setEditSubtaskName(true)}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    Edit
                                  </p>
                                </button>
                              </div>}

                          </div>
                          <div>

                          </div>
                        </div>
                      }

                      {dataCard &&
                        <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-description'
                              className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                            >
                              Description
                            </label>
                          </div>
                          <div className='sm:col-span-2 flex justify-between pr-12'>
                            {dataCard.data.description ? (editDescription ?
                              <div className="flex justify-between w-full">
                                <div>
                                  <ReactMde
                                    value={newDescription}
                                    onChange={setNewDescription}
                                    selectedTab={selectedTabDescription}
                                    onTabChange={setSelectedTabDescription}
                                    generateMarkdownPreview={markdown =>
                                      Promise.resolve(converter.makeHtml(markdown))
                                    }
                                    loadSuggestions={loadSuggestions}
                                    childProps={{
                                      writeButton: {
                                        tabIndex: -1
                                      }
                                    }}
                                  />
                                  <div className="flex justify-end border border-ts-gray-500 py-2 px-5">
                                    <button type="button" className="m-1" onClick={() => setEditDescription(false)}>
                                      <p className="text-white bg-ts-aquagreen-600 py-1 px-4 rounded-md">
                                        Cancel
                                      </p>
                                    </button>
                                    <button type="button" className="m-1" onClick={() => submitEdit({ id: 'description' })}>
                                      <p className="text-white bg-ts-aquagreen-600 py-1 px-4 rounded-md">
                                        save
                                      </p>
                                    </button>
                                  </div>
                                </div>
                              </div>
                              :
                              <div className="flex justify-between w-full">
                                <MarkdownPreview source={dataCard && dataCard.data.description} />
                                <button type="button" onClick={() => setEditDescription(true)}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    Edit
                                  </p>
                                </button>
                              </div>)
                              :
                              (statusAddDescription ?
                                <div className="flex justify-between w-full">
                                  <div>
                                    <ReactMde
                                      value={newDescription}
                                      onChange={setNewDescription}
                                      selectedTab={selectedTabDescription}
                                      onTabChange={setSelectedTabDescription}
                                      generateMarkdownPreview={markdown =>
                                        Promise.resolve(converter.makeHtml(markdown))
                                      }
                                      loadSuggestions={loadSuggestions}
                                      childProps={{
                                        writeButton: {
                                          tabIndex: -1
                                        }
                                      }}
                                    />
                                    <div className="flex justify-end border border-ts-gray-500 py-2 px-5">
                                      <button type="button" className="m-1" onClick={() => setStatusAddDescription(false)}>
                                        <p className="text-white bg-ts-aquagreen-600 py-1 px-4 rounded-md">
                                          Cancel
                                        </p>
                                      </button>
                                      <button type="button" className="m-1" onClick={() => submitEdit({ id: 'description' })}>
                                        <p className="text-white bg-ts-aquagreen-600 py-1 px-4 rounded-md">
                                          save
                                        </p>
                                      </button>
                                    </div>
                                  </div>
                                </div>
                                :
                                <button type="button" onClick={() => setStatusAddDescription(true)} >
                                  <p className="underline text-ts-gray-600 text-base">
                                    Add
                                  </p>
                                </button>)}
                          </div>
                        </div>}


                      {dataCard &&
                        <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className=' text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Role
                            </label>
                          </div>
                          {editRole ?
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className='sm:col-span-2 w-full'>
                                <Listbox value={newRole} onChange={setNewRole}>
                                  <div className="mt-1 relative">
                                    <Listbox.Button className="relative w-full bg-white border border-ts-gray-600 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-0 focus:border-ts-gray-600 sm:text-sm">
                                      <span className="w-full inline-flex truncate">
                                        <span className="truncate">{newRole ? newRole.name : 'Your role in this project'}</span>
                                      </span>
                                      <span className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                                        <img src="chevron-down.svg" alt="chevron-down" />
                                      </span>
                                    </Listbox.Button>

                                    <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                                      <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                                        {role.map((el) => (
                                          <Listbox.Option
                                            key={el.username}
                                            className={({ active }) =>
                                              classNamesRole(
                                                active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                                                'cursor-default select-none relative py-2 pl-3 pr-9'
                                              )
                                            }
                                            value={el}
                                          >
                                            {({ newRole, active }) => (
                                              <>
                                                <div className="flex justify-between">
                                                  <span className={classNamesRole(newRole ? 'font-semibold' : 'font-normal', 'truncate')}>
                                                    {el.name}
                                                  </span>
                                                  <span className={classNamesRole(active ? 'text-ts-gray-700' : 'text-ts-gray-500', 'ml-2 truncate')}>
                                                    {el.username}
                                                  </span>
                                                </div>
                                              </>
                                            )}
                                          </Listbox.Option>
                                        ))}
                                      </Listbox.Options>
                                    </Transition>
                                  </div>
                                </Listbox>
                              </div>
                              <div className="flex">
                                <button type="button" className="m-1" onClick={() => setEditRole(false)}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    Cancel
                                  </p>
                                </button>
                                <button type="button" className="m-1" onClick={() => submitEdit({ id: 'role' })}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    save
                                  </p>
                                </button>
                              </div>
                            </div>
                            :
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <p>{dataCard && dataCard.data.role.name}</p>
                              <button type="button" onClick={() => setEditRole(true)}>
                                <p className="underline text-ts-gray-600 text-base">
                                  Edit
                                </p>
                              </button>
                            </div>
                          }
                        </div>}


                      {dataCard &&
                        <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className=' text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Assignee
                            </label>
                          </div>
                          {editAssignee ?
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className='sm:col-span-2 w-full'>
                                <Listbox value={newAssignee} onChange={setNewAssignee}>
                                  <div className="mt-1 relative">
                                    <Listbox.Button className="relative w-full bg-white border border-ts-gray-600 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-o  focus:border-ts-gray-600 sm:text-sm">
                                      <span className="flex items-center">
                                        {newAssignee && newAssignee.avatar && <img src={newAssignee.avatar} alt="" className="flex-shrink-0 h-6 w-6 rounded-full" />}
                                        <span className="ml-3 block truncate">{newAssignee ? newAssignee.name : 'Assign task to..'}</span>
                                      </span>
                                      <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                        <img src="chevron-down.svg" alt="chevron-down" />
                                      </span>
                                    </Listbox.Button>

                                    <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                                      <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                                        {people.map((person) => (
                                          <Listbox.Option
                                            key={person.id}
                                            className={({ active }) =>
                                              classNamesAssignee(
                                                active ? 'text-ts-gray-900 bg-ts-gray-200 border-l-2 border-ts-aquagreen-600' : 'text-ts-gray-900',
                                                'cursor-default select-none relative py-2 pl-3 pr-9'
                                              )
                                            }
                                            value={person}
                                          >
                                            {({ newAssignee, active }) => (
                                              <>
                                                <div className="flex items-center">
                                                  {person.avatar && <img src={person.avatar} alt={`${person.avatar}`} className="flex-shrink-0 h-6 w-6 rounded-full" />}
                                                  <span className={classNamesAssignee(newAssignee ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}>
                                                    <p className={`${!person.avatar && 'underline'}`}>
                                                      {person.name}
                                                    </p>
                                                  </span>
                                                </div>
                                              </>
                                            )}
                                          </Listbox.Option>
                                        ))}
                                      </Listbox.Options>
                                    </Transition>
                                  </div>
                                </Listbox>
                              </div>
                              <div className="flex">
                                <button type="button" className="m-1" onClick={() => setEditAssignee(false)}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    Cancel
                                  </p>
                                </button>
                                <button type="button" className="m-1" onClick={() => submitEdit({ id: 'assign' })}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    save
                                  </p>
                                </button>
                              </div>
                            </div>
                            :
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className="flex flex-col">
                              {dataCard && dataCard.data.subtaskAssignees.map((el)=>{return(<p key={el.profile.id}>- {el.profile.name}</p>)})}
                              </div>
                              <button type="button" onClick={() => setEditAssignee(true)}>
                                <p className="underline text-ts-gray-600 text-base">
                                  Edit
                                </p>
                              </button>
                            </div>
                          }

                        </div>
                      }


                      {dataCard &&
                        <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Estimated time
                            </label>
                          </div>
                          {editEstimate ?
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className='sm:col-span-2 flex items-center'>
                                <input
                                  type="number"
                                  onKeyPress={(event) => handleKeyPressTimeEstimate(event)}
                                  min="0"
                                  value={newEstimate.hour} onChange={(e) => setNewEstimate({ hour: e.target.value, minute: newEstimate.minute })}
                                  className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                                />

                                <p className="mx-1">
                                  hour(s)
                                </p>
                                <input
                                  type="number"
                                  onKeyPress={(event) => handleKeyPressTimeEstimate(event)}
                                  min="0"
                                  value={newEstimate.minute} onChange={(e) => setNewEstimate({ hour: newEstimate.hour, minute: e.target.value })}
                                  className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                                />
                                <p className=" mx-1">
                                  minute(s)
                                </p>
                              </div>
                              <div className="flex">
                                <button type="button" className="m-1" onClick={() => setEditEstimate(false)}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    Cancel
                                  </p>
                                </button>
                                <button type="button" className="m-1" onClick={() => submitEdit({ id: 'estimate' })}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    save
                                  </p>
                                </button>
                              </div>
                            </div>
                            :
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className="flex">
                                <p className="text-base">
                                {dataCard && ~~(Number(dataCard.data.estimatedTime)/60)}
                                </p>

                                <p className="mx-2 text-ts-gray-600">
                                  hour(s)
                                </p>
                                <p className="text-base ml-4 ">
                                  {dataCard && Number(dataCard.data.estimatedTime)%60}
                                </p>
                                <p className=" mx-2 text-ts-gray-600">
                                  minute(s)
                                </p>
                              </div>
                              <button type="button" onClick={() => setEditEstimate(true)}>
                                <p className="underline text-ts-gray-600 text-base">
                                  Edit
                                </p>
                              </button>
                            </div>
                          }
                        </div>
                      }


                      {/* {dataCard && dataCard.items.pastTask &&
                        <div className='flex items-center space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                          <div>
                            <label
                              htmlFor='group-name'
                              className='text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                            >
                              Time spent
                            </label>
                          </div>
                          {editSpent ?
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className='sm:col-span-2 flex items-center'>
                                <input
                                  type="number"
                                  onKeyPress={(event) => handleKeyPressTimeSpent(event)}
                                  min="0"
                                  value={newSpent.hour} onChange={(e) => setNewSpent({ hour: e.target.value, minute: newSpent.minute })}
                                  className="shadow-sm text-base text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                                />

                                <p className="mx-1">
                                  hour(s)
                                </p>
                                <input
                                  type="number"
                                  onKeyPress={(event) => handleKeyPressTimeSpent(event)}
                                  min="0"
                                  value={newSpent.minute} onChange={(e) => setNewSpent({ hour: newSpent.hour, minute: e.target.value })}
                                  className="shadow-sm text-base ml-4 text-ts-gray-600 block focus:border-gray-500 focus:border focus:ring-0 font-normal w-12 h-8 rounded-md px-1 "
                                />
                                <p className=" mx-1">
                                  minute(s)
                                </p>
                              </div>
                              <div className="flex">
                                <button type="button" className="m-1" onClick={() => setEditSpent(false)}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    Cancel
                                  </p>
                                </button>
                                <button type="button" className="m-1" onClick={() => submitEdit({ id: 'spent' })}>
                                  <p className="underline text-ts-gray-600 text-base">
                                    save
                                  </p>
                                </button>
                              </div>
                            </div>
                            :
                            <div className='sm:col-span-2 flex justify-between pr-12'>
                              <div className="flex">
                                <p className="text-base">
                                  {newDataCard ?
                                    (newDataCard.spent.hour ? newDataCard.spent.hour : '0')
                                    :
                                    (dataCard.items.spent.hour ? dataCard.items.spent.hour : '0')}
                                </p>

                                <p className="mx-2 text-ts-gray-600">
                                  hour(s)
                                </p>
                                <p className="text-base ml-4 ">
                                  {newDataCard ?
                                    (newDataCard.spent.minute ? newDataCard.spent.minute : '0')
                                    :
                                    (dataCard.items.spent.minute ? dataCard.items.spent.minute : '0')}
                                </p>
                                <p className=" mx-2 text-ts-gray-600">
                                  minute(s)
                                </p>
                              </div>
                              <button type="button" onClick={() => setEditSpent(true)}>
                                <p className="underline text-ts-gray-600 text-base">
                                  Edit
                                </p>
                              </button>
                            </div>
                          }
                        </div>
                      } */}
                    </div>
                  </div>
                  {/* {dataCard &&
                    <div className="flex flex-col items-center px-4 py-5">
                      {dataCard.items.comment && dataCard.items.comment.length > 0 &&
                        <div className="w-full px-8 mb-8">
                          <p className="font-normal text-base">
                            Comment
                          </p>
                          <div className="mt-2">
                            {newCommentsBoard ? newCommentsBoard.map((element, index) => {
                              return (
                                <div key={index} className=" flex  w-full py-2 px-3 bg-ts-white-100 border border-ts-gray-500 rounded-md my-2 ">
                                  <div className="px-2">
                                    <img className="w-6 h-6 rounded-full" src={element.image} alt={element.image} />
                                  </div>
                                  <div className="ml-3 w-full">
                                    <div className="flex justify-between w-full">
                                      <p>{element.name}</p>
                                      {!statusEditComments.status && !(statusEditComments.index === index) && <button type="button" onClick={() => (setstatusEditComments({ status: true, index: index }), setnewEditComments(''))}>
                                        <img src="edit-icon.svg" alt="edit-icon" />
                                      </button>}
                                    </div>
                                    {statusEditComments.status && (statusEditComments.index === index)
                                      ? <div className="px-5 py-7">
                                        <ReactMde
                                          value={newEditComments ? newEditComments : element.description}
                                          onChange={setnewEditComments}
                                          selectedTab={selectedTabEditComments}
                                          onTabChange={setSelectedTabEditComments}
                                          generateMarkdownPreview={markdown =>
                                            Promise.resolve(converter.makeHtml(markdown))
                                          }
                                          loadSuggestions={loadSuggestions}
                                          childProps={{
                                            writeButton: {
                                              tabIndex: -1
                                            }
                                          }}
                                        />
                                      </div>
                                      :
                                      <div>
                                        <MarkdownPreview source={element.description} />
                                      </div>}
                                  </div>
                                  {statusEditComments.status && (statusEditComments.index === index) &&
                                    <div>
                                      <div className="flex justify-between">
                                        <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => (setstatusEditComments({ status: false, index: '' }), setnewEditComments(''))}>
                                          cancel
                                        </button>
                                        <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => submitEditComment(index)}>
                                          save
                                        </button>
                                      </div>
                                    </div>
                                  }
                                </div>
                              )
                            }) : dataCard.items.comment.map((element, index) => {
                              return (
                                <div key={index} className=" flex  w-full py-2 px-3 bg-ts-white-100 border border-ts-gray-500 rounded-md my-2 ">
                                  <div className="px-2">
                                    <img className="w-6 h-6 rounded-full" src={element.image} alt={element.image} />
                                  </div>
                                  <div className="ml-3 w-full">
                                    <div className="flex justify-between w-full">
                                      <p>{element.name}</p>
                                      {!statusEditComments.status && !(statusEditComments.index === index) && <button type="button" onClick={() => (setstatusEditComments({ status: true, index: index }), setnewEditComments(''))}>
                                        <img src="edit-icon.svg" alt="edit-icon" />
                                      </button>}
                                    </div>
                                    {statusEditComments.status && (statusEditComments.index === index)
                                      ? <div className="px-5 py-7">
                                        <ReactMde
                                          value={newEditComments ? newEditComments : element.description}
                                          onChange={setnewEditComments}
                                          selectedTab={selectedTabEditComments}
                                          onTabChange={setSelectedTabEditComments}
                                          generateMarkdownPreview={markdown =>
                                            Promise.resolve(converter.makeHtml(markdown))
                                          }
                                          loadSuggestions={loadSuggestions}
                                          childProps={{
                                            writeButton: {
                                              tabIndex: -1
                                            }
                                          }}
                                        />
                                      </div>
                                      :
                                      <div>
                                        <MarkdownPreview source={element.description} />
                                      </div>}
                                  </div>
                                  {statusEditComments.status && (statusEditComments.index === index) &&
                                    <div>
                                      <div className="flex justify-between">
                                        <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => (setstatusEditComments({ status: false, index: '' }), setnewEditComments(''))}>
                                          cancel
                                        </button>
                                        <button className="mx-2 underline text-ts-gray-600 text-base" type="button" onClick={() => submitEditComment(element.id)}>
                                          save
                                        </button>
                                      </div>
                                    </div>
                                  }
                                </div>
                              )
                            })}
                          </div>
                        </div>
                      }
                      <div>
                        <ReactMde
                          value={comments}
                          onChange={setComments}
                          selectedTab={selectedTabComment}
                          onTabChange={setSelectedTabComment}
                          generateMarkdownPreview={markdown =>
                            Promise.resolve(converter.makeHtml(markdown))
                          }
                          loadSuggestions={loadSuggestions}
                          childProps={{
                            writeButton: {
                              tabIndex: -1
                            }
                          }}
                        />
                        <div className="flex justify-end border border-ts-gray-500 py-2 px-5">
                          <button onClick={() => handleSubmitCommentSubtask()} type="button" className="text-white bg-ts-aquagreen-600 py-1 px-4 rounded-md">
                            comments
                          </button>
                        </div>
                      </div>
                    </div>
                  } */}

                </form>
              </div>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

export default subtaskViewForm
