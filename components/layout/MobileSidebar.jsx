import React from 'react'
import { classNames } from '../../utilities/css-util'
import { useSession } from "next-auth/client"

const MobileSidebar = ({ navigation, adminNavigation }) => {
  const [session, loading] = useSession()

  return (
    <>
      <a href='/timesheet' className="flex-shrink-0 flex items-center px-4">
        <img
          className="w-auto"
          src="logo-white.png"
        />
      </a>
      <div className="mt-5 flex-1 h-0 overflow-y-auto">
        <nav className="px-2 space-y-1">
          {navigation.map((item) => (
            <a
              key={item.name}
              href={item.href}
              className={classNames(
                item.current ? 'bg-ts-blue-800 text-white' : 'text-gray-300 hover:bg-ts-blue-700 hover:text-white',
                'group flex items-center px-2 py-2 text-base font-medium rounded-md'
              )}
            >
              <item.icon
                className={classNames(
                  item.current ? 'text-gray-300' : 'text-gray-400 group-hover:text-gray-300',
                  'mr-4 flex-shrink-0 h-6 w-6'
                )}
                aria-hidden="true"
              />
              {item.name}
            </a>
          ))}

          {session?.user?.admin && <>
            <div className='border-t border-gray-600' />
            {adminNavigation.map((item) => (
              <a
                key={item.name}
                href={item.href}
                className={classNames(
                  item.current ? 'bg-ts-blue-800 text-white' : 'text-gray-300 hover:bg-ts-blue-700 hover:text-white',
                  'group flex items-center px-2 py-2 text-base font-medium rounded-md'
                )}
              >
                <item.icon
                  className={classNames(
                    item.current ? 'text-gray-300' : 'text-gray-400 group-hover:text-gray-300',
                    'mr-4 flex-shrink-0 h-6 w-6'
                  )}
                  aria-hidden="true"
                />
                {item.name}
              </a>
            ))}
          </>}
        </nav>
      </div>
    </>
  )
}

export default MobileSidebar
