import { Fragment } from 'react'
import { classNames } from '../../utilities/css-util'
import { Menu, Transition } from '@headlessui/react'
import { signOut, useSession } from "next-auth/client"

const UserMenu = ({ userNavigation }) => {
  const [session, loading] = useSession()

  return (
    <>
      <Menu as="div" className="ml-3 relative">
        <div>
          <Menu.Button className="max-w-xs bg-white flex items-center rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 space-x-2">
            {session?.user?.admin && <div className='bg-blue-700 text-white px-2 py-1 rounded-xl text-xs'>ADMIN</div>}
            <img
              className="h-8 w-8 rounded-full"
              src={session?.user?.picture}
              alt=""
            />
            <div>{session?.user?.name}</div>
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-3 w-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
              </svg>
            </div>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
            {userNavigation.map((item) => (
              <Menu.Item key={item.name}>
                {({ active }) => (
                  <a
                    href={item.href}
                    className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700')}
                  >
                    {item.name}
                  </a>
                )}
              </Menu.Item>
            ))}

            <Menu.Item>
              <a onClick={signOut} className='block px-4 py-2 text-sm text-gray-700 cursor-pointer hover:bg-gray-100'>Log out</a>
            </Menu.Item>
          </Menu.Items>
        </Transition>
      </Menu>
    </>
  )
}

export default UserMenu
