import { useState, Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon, DocumentAddIcon } from '@heroicons/react/outline'
import { Listbox } from '@headlessui/react'
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid'

import ReactMde from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";

function loadSuggestions(text) {
    return new Promise((accept, reject) => {
        setTimeout(() => {
            const suggestions = [
                {
                    preview: "Andre",
                    value: "@andre"
                },
                {
                    preview: "Angela",
                    value: "@angela"
                },
                {
                    preview: "David",
                    value: "@david"
                },
                {
                    preview: "Louise",
                    value: "@louise"
                }
            ].filter(i => i.preview.toLowerCase().includes(text.toLowerCase()));
            accept(suggestions);
        }, 250);
    });
}

const converter = new Showdown.Converter({
    tables: true,
    simplifiedAutoLink: true,
    strikethrough: true,
    tasklists: true
});

const TaskCreateForm = ({ open, onClose, onSave, projectData }) => {
    const [description, setDescription] = useState('');
    const [selectedTab, setSelectedTab] = useState("write");
    const [role, setRole] = useState('')
    const [taskName, setTaskName] = useState('')
    const myCurrentDate = new Date();
    const date = myCurrentDate.getFullYear() + '/' + (myCurrentDate.getMonth() + 1) + '/' + myCurrentDate.getDate() + ' ' + myCurrentDate.getHours() + ':' + myCurrentDate.getMinutes() + ':' + myCurrentDate.getSeconds();
    const handleSubmit = async (event) => {
        event.preventDefault()
        try {
            const result = await fetch('/api/task/createtask', {
                method: 'post',
                body: JSON.stringify(
                    {
                        "name": taskName,
                        description,
                        "project": {
                            "id": projectData.id
                        },
                        "role": {
                            "id": "abe40b26-8960-428d-9eba-fd9141c28f69"
                        }
                    }
                )
            })
            const data = await result.json()
            onSave(data)
        } catch (error) {
            console.error(error)
        }
    }

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as='div' className='fixed inset-0 overflow-hidden z-10' onClose={onClose}>
                <div className='absolute inset-0 overflow-hidden'>
                    <Dialog.Overlay className='absolute inset-0' />

                    <div className='fixed inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16'>
                        <Transition.Child
                            as={Fragment}
                            enter='transform transition ease-in-out duration-500 sm:duration-700'
                            enterFrom='translate-x-full'
                            enterTo='translate-x-0'
                            leave='transform transition ease-in-out duration-500 sm:duration-700'
                            leaveFrom='translate-x-0'
                            leaveTo='translate-x-full'
                        >
                            <div className='w-screen max-w-2xl'>
                                <form className='h-full flex flex-col bg-white shadow-xl overflow-y-scroll' onSubmit={handleSubmit}>
                                    <div className='flex-1'>
                                        {/* Header */}
                                        <div className='px-4 py-6 sm:px-6 bg-ts-aquagreen-100'>
                                            <div className='flex items-start justify-between space-x-3'>
                                                <div className='space-y-1'>
                                                    <Dialog.Title className='text-lg font-medium text-gray-900 flex space-x-2 items-center'>
                                                        <p><DocumentAddIcon className='w-6 h-6' /></p>
                                                        <p>Add task</p>
                                                    </Dialog.Title>
                                                </div>
                                                <div className='h-7 flex items-center'>
                                                    <button
                                                        type='button'
                                                        className='text-gray-400 hover:text-gray-500'
                                                        onClick={onClose}
                                                    >
                                                        <span className='sr-only'>Close panel</span>
                                                        <XIcon className='h-6 w-6' aria-hidden='true' />
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        {/* Divider container */}
                                        <div className='py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200'>

                                            {/* Create at */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Create at
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2'>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        {date}
                                                    </label>
                                                </div>
                                            </div>

                                            {/* Project */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Project
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2'>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-400 sm:mt-px sm:pt-2'
                                                    >
                                                        {projectData && projectData.name}
                                                    </label>
                                                </div>
                                            </div>
                                            {/* role */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                                                    >
                                                        Role
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2'>
                                                    <select class="inputbox rounded-md w-full mt-1"
                                                        onChange={e => setRole(e.target.value)}
                                                        required
                                                    >
                                                        <option value="" disabled selected>Assign task to role</option>
                                                        <option className='hover:text-red-300' value="BD">BD </option>
                                                        <option value="CM">CM</option>
                                                        <option value="DevOps">DevOps</option>
                                                        <option value="PM">PM</option>
                                                        <option value="QA">QA</option>
                                                        <option value="SA">SA</option>
                                                        <option value="SWE">SWE</option>
                                                        <option value="SWT">SWT</option>
                                                        <option value="UXUI">UXUI</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* taskname */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-name'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2 required'
                                                    >
                                                        Task name
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2'>
                                                    <input type='text' className='inputbox w-full' onChange={e => setTaskName(e.target.value)} required />
                                                </div>
                                            </div>
                                            {/* description */}
                                            <div className='space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5'>
                                                <div>
                                                    <label
                                                        htmlFor='group-description'
                                                        className='block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2'
                                                    >
                                                        Description
                                                    </label>
                                                </div>
                                                <div className='sm:col-span-2'>
                                                    <ReactMde
                                                        value={description}
                                                        onChange={setDescription}
                                                        selectedTab={selectedTab}
                                                        onTabChange={setSelectedTab}
                                                        generateMarkdownPreview={markdown =>
                                                            Promise.resolve(converter.makeHtml(markdown))
                                                        }
                                                        loadSuggestions={loadSuggestions}
                                                        childProps={{
                                                            writeButton: {
                                                                tabIndex: -1
                                                            }
                                                        }}
                                                    />
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    {/* Action buttons */}
                                    <div className='flex-shrink-0 px-4 border-t border-gray-200 py-5 sm:px-6'>
                                        <div className='space-x-3 flex justify-end'>
                                            <button
                                                type='button'
                                                className='bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                                onClick={onClose}
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                type='submit'
                                                className='inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-ts-aquagreen-600 hover:bg-ts-aquagreen-700'
                                            >
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    )
}

export default TaskCreateForm
