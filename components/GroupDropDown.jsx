import { Fragment, useState, useEffect } from 'react'
import { useSession } from "next-auth/client"
import { Listbox, Transition } from '@headlessui/react'
import { classNames } from '../utilities/css-util'
import {
  UserAddIcon,
  ChevronDownIcon,
  CheckIcon,
} from '@heroicons/react/solid'

export default function DropDown({ availableGroup, groupSelected, setGroupSelected }) {
  const [session, loading] = useSession()
  const [search, setSearch] = useState('')


  return (
    <Listbox value={groupSelected.id} onChange={setGroupSelected}>
      <Listbox.Label className="block text-sm font-medium text-gray-700">Select group</Listbox.Label>
      <div className="mt-1 relative">

        <Listbox.Button className="bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
          <span className="block truncate">{groupSelected.name}</span>
          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
            <ChevronDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
          </span>
        </Listbox.Button>

        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
          className="absolute mt-1 w-full rounded-md bg-white shadow-lg"
        >

          <Listbox.Options
            className="absolute z-50 mt-1 w-full bg-white shadow-lg max-h-60 rounded-t-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
          >

            <div className='flex p-2 space-x-4'>
              <input type='text' className='inputbox w-full' value={search} placeholder='search' onChange={e => setSearch(e.target.value)} />
            </div>

            {availableGroup.filter((group) => group.name.toLowerCase().includes(search)).map((group) => (
              <Listbox.Option
                key={group.id}
                value={group}
                onClick={() => console.log(group.name)}
                className={({ selected, active }) => classNames(
                  selected ? (
                    active ? 'bg-ts-gray-200' : 'bg-ts-aquagreen-100')
                    : (
                      active ? 'bg-ts-gray-200' : '')
                  , 'cursor-default select-none relative py-2 pl-3 pr-9'
                )}
              >
                {({ selected, active }) => (
                  <div className='grid grid-cols-10'>
                    {selected ? (
                      <span
                        className='col-span-1 flex items-center text-ts-aquagreen-600'
                      >
                        <CheckIcon className="h-5 w-5" aria-hidden="true" />
                      </span>
                    ) : <span
                      className='col-span-1 flex items-center text-transparent'
                    >
                      <CheckIcon className="h-5 w-5" aria-hidden="true" />
                    </span>}

                    <span className='col-span-6 flex items-center truncate ml-2'>
                      {group.name}
                    </span>

                  </div>
                )}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </Transition>
      </div>
    </Listbox>
  )
}