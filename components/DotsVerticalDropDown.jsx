import { useState, Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { classNames } from '../utilities/css-util'
import {
  DotsVerticalIcon,
  ChevronDownIcon,
  PlusIcon
} from '@heroicons/react/solid'

import {
  EyeIcon,
  EyeOffIcon,
  PencilAltIcon,
  TrashIcon,
  UserAddIcon,
} from '@heroicons/react/outline'
import { identity } from 'lodash'
import { useRouter } from 'next/router'


const DotsVerticalDropDown = ({ prefix, id, newOption, manageUserOption, handleCreateProject, viewGroup, viewProject }) => {
  const router = useRouter()
  const {pathname} = router

  const [groupData, setGroupData] = useState()
  const [projectData, setProjectData] = useState()


  const handleView = () =>{
    if(pathname === '/group'){
      viewGroup(groupData)
    }
    if(pathname === '/project'){
      viewProject(projectData)
    }
  }

  const handleGet = (id) =>{
    if(pathname === '/group'){
      getGroupByID(id)
    }
    if(pathname === '/project'){
      getProjectByID(id)
    }
  }

  const getProjectByID = async (id) => {
    try {
      const response = await fetch(`api/project/viewprojectbyid?project_id=${id}`)
      const data = await response.json()
      setProjectData(data)
    } catch (error) {
      console.log(error);
    }
  }

  const getGroupByID = async (id) => {
    try {
      const response = await fetch(`api/group/viewgroupbyid?group_id=${id}`)
      const data = await response.json()
      setGroupData(data)
    } catch (error) {
      console.log(error);
    }
  }

  const handleCreateProjecybyGroup = () =>{
    handleCreateProject(groupData)
  }

  return (
    <Menu as="div" className="relative inline-block text-left">
      <div>
        <Menu.Button className="flex justify-center z-0">
          <div className='hover:bg-gray-200 rounded-lg p-1 cursor-pointer'>
            <button onClick={() => handleGet(id)}>
              <DotsVerticalIcon className='w-6 h-6' />
            </button>
          </div>
        </Menu.Button>
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="origin-top-right absolute right-0 mt-2 rounded-md bg-white ring-1 ring-gray-400 z-20">
          <div className="p-2">
            {newOption && (
              <Menu.Item>
                {({ active }) => (
                  <button className={classNames(
                    active ? 'bg-gray-200 rounded-md' : '',
                    'block px-4 py-2 border-b-2 border-gray-400'
                  )}
                    onClick={() => (
                      handleCreateProjecybyGroup()
                    )}>
                    <div className='flex space-x-4'>
                      <PlusIcon className='inline w-6 h-6' /><span className='text-xs'>New Project</span>
                    </div>
                  </button>
                )}
              </Menu.Item>
            )}
            <Menu.Item>
              {({ active }) => (
                <button onClick={() => handleView()} className={classNames(
                  active ? 'bg-gray-200 rounded-md' : '',
                  'block px-4 py-2'
                )}>
                  <div className='flex space-x-4'>
                    <EyeIcon className='inline w-6 h-6' /><span>View</span>
                  </div>
                </button>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <a
                  href={`${prefix}/edit/${id}`}
                  className={classNames(
                    active ? 'bg-gray-200 rounded-md' : '',
                    'block px-4 py-2'
                  )}
                >
                  <div className='flex space-x-4'>
                    <PencilAltIcon className='inline w-6 h-6' /><span>Edit</span>
                  </div>
                </a>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <a
                  href={`${prefix}/hide/${id}`}
                  className={classNames(
                    active ? 'bg-gray-200 rounded-md' : '',
                    'block px-4 py-2'
                  )}
                >
                  <div className='flex space-x-4'>
                    <EyeOffIcon className='inline w-6 h-6' /><span>Hide</span>
                  </div>
                </a>
              )}
            </Menu.Item>
            {manageUserOption && (
              <Menu.Item>
                {({ active }) => (
                  <a
                    href={'/manage_user'}
                    className={classNames(
                      active ? 'bg-gray-200 rounded-md' : '',
                      'block px-4 py-2 border-b-2 border-gray-400'
                    )}
                  >
                    <div className='flex space-x-4'>
                      <UserAddIcon className='inline w-6 h-6' /><span>Manage User</span>
                    </div>
                  </a>
                )}
              </Menu.Item>
            )}
            <Menu.Item>
              {({ active }) => (
                <a
                  href={`${prefix}/delete/${id}`}
                  className={classNames(
                    active ? 'bg-gray-200 rounded-md' : '',
                    'block px-4 py-2'
                  )}
                >
                  <div className='flex space-x-4'>
                    <TrashIcon className='inline w-6 h-6' /><span>Delete</span>
                  </div>
                </a>
              )}
            </Menu.Item>
          </div>
        </Menu.Items>
      </Transition>
    </Menu >
  )
}

export default DotsVerticalDropDown