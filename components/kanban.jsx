import React, { Fragment, useState, useEffect } from "react"
import { useForm } from "react-hook-form";
import { Listbox, Transition, Switch } from '@headlessui/react'
import { useSession } from "next-auth/client"
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd"
import SubtaskCreateForm from './SubtaskCreateForm'
import SubtaskViewForm from './SubtaskViewForm'
import _, { map, set } from 'lodash'
import { v4 } from "uuid"
import { useRouter } from 'next/router'

const kanban = ({subtasks,taskid,taskdata,projectId}) => {
  const router = useRouter()
  const [session, loading] = useSession()
  const [showViewForm, setShowViewForm] = useState(false)
  const [showCreateForm, setShowCreateForm] = useState(false)
  const [showSubtask, setShowSubtask] = useState({ title: '', id: '' })
  const [dataSubtask, setDataSubtask] = useState()
  const [availableMember, setAvailableMember] = useState([])
  const [state, setState] = useState({})
  const [listAllTask, setListAllTask] = useState([])

  const getAllTaskByProjectId = async () => {
    try {
      const response = await fetch(`api/project/viewprojectbyid?project_id=${projectId}`)
      const data = await response.json()
      const listData = data.tasks.map(el => el)
      setListAllTask(listData)
      
    } catch (error) {
      console.log(error);
    }
  }

  const loadAvailableMembers = async () => {
    const url = `/api/profile/`
    try {
      const result = await fetch(url, { method: 'GET' })
      const data = await result.json()
      setAvailableMember(data)
    } catch (error) {
      console.error(error)
    }
  }
  
  const getKanbanByTaskID = async () => {
    try {
      const response = await fetch(`api/task/viewkanbanbytaskid?task_id=${taskid}`)
      const data = await response.json()
      setState(data)
    } catch (error) {
      console.log(error);
    }
  }

  const handleDragEnd = async ({ destination, source }) => {
    if (!destination) {
      console.log('not droped in dropable');
      return
    }
    if (destination.index === source.index && destination.droppableId === source.droppableId) {
      console.log('droped in sameplace ');
      return
    }
    //create copyitem before removing it form state
    const itemCopy = { ...state[source.droppableId][source.index] }
    setState(prev => {
      prev = { ...prev }
      //remove item form array
      prev[source.droppableId].splice(source.index, 1)

      //adding item to new array
      prev[destination.droppableId].splice(destination.index, 0, itemCopy)

      return prev
    })
    if(destination.droppableId){
      try {
        const response = await fetch(`api/subtask/dndcard?subtask_id=${itemCopy.id}&to_be_status=${destination.droppableId}`)
        const data = await response.json()
        if (data) {
          getKanbanByTaskID()
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

  const handleSubmitSubtask = async (value) => {
    try {
      const response = await fetch('/api/subtask/createsubtask', {
          method: 'post',
          body: JSON.stringify(
            value
          )
      })
      const data = await response.json()
    } catch (error) {
      console.error(error)
    }
    getKanbanByTaskID()
  }

  const handleViewSubtaskForm = async (id,key) => {
    try {
      const response = await fetch(`api/subtask/viewsubtaskbyid?subtask_id=${id}`)
      const data = await response.json()
      setDataSubtask({data:data,key:key})
      setShowViewForm(true)
    } catch (error) {
      console.log(error);
    }
  }

  const handleChangeTask = (value) =>{
    router.push({
      pathname: `/subtask`,
      query: {
        id: value,
        project_id:projectId
      }
    })
  }

  const addComment = async (value, comments) => {
    try {
      const response = await fetch('/api/subtask/viewsubtaskbyid', {
        method: 'POST',
        body: JSON.stringify({
          idCard: value.id,
          idSubtask: value.items.id,
          id: v4(),
          name: session.user.name,
          description: comments,
          image: session.user.picture
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      const indexSubtask = state[value.id].items.findIndex(element => element.id === value.items.id);
      const result = state[value.id].items[indexSubtask]
      const idSubtask = state[value.id]
      const dataPack = {
        showViewForm: true,
        id: idSubtask.id,
        title: idSubtask.title,
        items:
        {
          id: result.id,
          name: result.name,
          time: result.time,
          assign: result.assign,
          description: result.description,
          role: result.role,
          estimate: result.estimate,
          spent: result.spent,
          pastTask: result.pastTask,
          comment: result.comment
        }
      }
      const data = await response.json()
      if (data) {
        // getSubtask()
        setDataSubtask(dataPack)
      }

    } catch (error) {
      console.log(error);
    }
  }

  const submitEdit = async (value) => {
    try {
      const response = await fetch('/api/subtask/edit', {
        method: 'POST',
        body: JSON.stringify(value),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      // getSubtask()

    } catch (error) {
      console.log(error);
    }
  }

  const submitEditComment = async (value) => {
    try {
      const response = await fetch('/api/subtask/comment/edit', {
        method: 'POST',
        body: JSON.stringify(value),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      // getSubtask()

    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    loadAvailableMembers()
    getAllTaskByProjectId()
    if (subtasks) {
    setState(subtasks)
    }
  }, [subtasks])

  return (
    <div className="flex flex-col w-full">
      {showCreateForm && <div onClick={() => setShowCreateForm(false)} className="w-full h-screen bg-ts-blue-900 absolute -top-0 -left-0 opacity-20" />}
      <SubtaskCreateForm taskdata={taskdata} open={showCreateForm} onClose={() => setShowCreateForm(false)} availableMember={availableMember} onSave={handleSubmitSubtask} />
      {showViewForm && <div onClick={() => setShowViewForm(false)} className="w-full h-screen bg-ts-blue-900 absolute -top-0 -left-0 opacity-20" />}
      <SubtaskViewForm taskdata={taskdata} dataCard={dataSubtask} open={showViewForm} onClose={() => setShowViewForm(false)} onSaveComment={addComment} editData={submitEdit} editComment={submitEditComment} />
      <div className="px-10 font-normal text-lg text-ts-gray-800">
      <div className='sm:col-span-2'>
                <select class="w-max mt-1 ring-0 border-0 shadow-none focus:ring-0 focus:border-0" onChange={e => handleChangeTask(e.target.value)}>
\                    {listAllTask&&listAllTask.map((el)=>{if(el.name === taskdata.taskName){return(
                    <option value="" disabled selected>{el.name}</option>
                    )}})}
                    {listAllTask&&listAllTask.map((el)=>{if(el.name !== taskdata.taskName){return(
                    <option className='hover:text-red-300' value={el.id}>{el.name} </option>
                    )}})}
                </select>
      </div>
      </div>
      <div className="flex w-full h-full mt-7 px-3 overflow-x-scroll">
        <DragDropContext onDragEnd={handleDragEnd} >
        {_.map(state, (data, key) => {
            return (
              <div key={key} className="w-full h-full mx-3 px-2 py-5 bg-ts-gray-100 shadow-inner border border-ts-gray-300 rounded-md">
                <div className="flex justify-between">
                  <div className="flex items-center">
                    <div className={`mx-2 px-4 py-1 rounded-md text-white flex w-max ${key === 'todo' ? 'bg-ts-yellow-500' : key === 'doing' ? 'bg-ts-green-400' : key === 'done' ? 'bg-ts-blue-500' : 'bg-ts-red-600'}`}>
                      {key}
                    </div>
                    <div className="ml-2 font-normal text-lg text-ts-white-400">
                      ({data.length})
                    </div>
                  </div>
                  {key === 'todo' ?
                    <button onClick={() => setShowCreateForm(true)} className="w-7 h-7 border border-ts-gray-500 rounded-md p-2 hover:bg-gray-500">
                      <img className="w-full h-full" alt="add-icon-black" src="add-icon-black.svg" />
                    </button> : null}
                </div>
                <div className="h-height-card overflow-y-scroll">
                  <Droppable droppableId={key} >
                    {(provided) => {
                      return (
                        <div
                          ref={provided.innerRef}
                          {...provided.droppableProps}
                          className="w-96 h-height-card mt-6"
                        >
                          {data.map((el, index) => {
                            return (
                              <Draggable key={el.id} index={index} draggableId={el.id}>
                                {(provided, snapshot) => {
                                  return (
                                    <div
                                      className={`w-full flex flex-col justify-between h-32 rounded-lg ${snapshot.isDragging ? 'bg-ts-gray-200' : 'bg-white'} border border-ts-gray-300 my-2`}
                                      ref={provided.innerRef}
                                      {...provided.draggableProps}
                                      {...provided.dragHandleProps}
                                    >
                                      <button onClick={()=>handleViewSubtaskForm(el.id,key)} className="font-normal text-left w-max underline text-base mx-2 my-2">
                                        {el.name}
                                      </button>
                                      <div className="font-normal text-xs px-3 py-3 border-t border-ts-gray-300 text-ts-gray-500 flex justify-between items-center">
                                      <p>
                                          Create at : {new Date(`${el.createdDate}`).toLocaleDateString()}
                                      </p>
                                        {el.subtaskAssignees.length > 0 && 
                                        <div className="mr-3 flex font-normal text-base text-ts-gray-900">
                                            <div className='flex col-span-1 items-center justify-center'>
                                                <div className="-space-x-2">
                                                    {el.subtaskAssignees.map((el,index)=>{
                                                        if(index <= 1){
                                                            return(
                                                                <img key={el.profile.id} className="z-30 inline object-cover w-8 h-8 border-2 border-white rounded-full" src={el.profile.picture} alt="Profile image" />
                                                        )}})
                                                    }
                                                </div>
                                            </div>
                                            <div className="flex items-center ">
                                              <p>
                                                {el.subtaskAssignees.length > 2 ? `+${el.subtaskAssignees.length - 2}` : ""}
                                              </p>
                                            </div>
                                        </div>
                                        }
                                      </div>
                                    </div>
                                  )
                                }}
                              </Draggable>
                            )
                          })}
                          {provided.placeholder}
                        </div>
                      )
                    }}
                  </Droppable>
                </div>
              </div>
            )
          })}
        </DragDropContext>
      </div>
    </div>
  )
}

export default kanban