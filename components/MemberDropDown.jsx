import { Fragment, useState, useEffect } from 'react'
import { useSession } from "next-auth/client"
import { Listbox, Transition } from '@headlessui/react'
import { classNames } from '../utilities/css-util'
import {
  UserAddIcon,
  ChevronDownIcon,
  CheckIcon,
} from '@heroicons/react/solid'

export default function MemberDropDown({ availableMember, projectManager, setProjectManager }) {
  const [session, loading] = useSession()
  const [search, setSearch] = useState('')


  return (
    <Listbox value={projectManager.name} onChange={setProjectManager}>
      <div className="w-full relative">

        <Listbox.Button className="bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
          <span className="block truncate">{projectManager.name}</span>
          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
            <ChevronDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
          </span>
        </Listbox.Button>

        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
          className="absolute mt-1 w-full rounded-md bg-white shadow-lg"
        >

          <Listbox.Options
            className="absolute z-50 mt-1 w-full bg-white shadow-lg max-h-60 rounded-t-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm"
          >
            <Listbox.Option
              key={session?.user?.id}
              value={session?.user}
              className={({ selected, active }) => classNames(
                selected ? (
                  active ? 'bg-ts-gray-200' : 'bg-ts-aquagreen-100')
                  : (
                    active ? 'bg-ts-gray-200' : '')
                , 'cursor-default select-none relative border-b-2 border-ts-gray-400 py-2 pl-3 pr-9'
              )}
            >
              {({ selected, active }) => (
                <div className='grid grid-cols-10'>
                  <span className='col-span-8 items-center flex truncate underline'>
                    Assign yourself
                  </span>
                  <span className='col-span-2 items-center flex-inline text-right text-gray-400'>
                    16 Task
                    <p className='text-xs'>on progress</p>
                  </span>
                </div>
              )}

            </Listbox.Option>
            <div className='flex p-2 space-x-4'>
              <UserAddIcon className='h-8 w-8 text-ts-gray-400'> </UserAddIcon>
              <input type='text' className='inputbox w-full' value={search} placeholder='search' onChange={e => setSearch(e.target.value)} />
            </div>

            {availableMember.filter((person) => person.name.toLowerCase().includes(search)).map((person) => (
              <Listbox.Option
                key={person.id}
                value={person}
                onClick={() => console.log(person.name)}
                className={({ selected, active }) => classNames(
                  selected ? (
                    active ? 'bg-ts-gray-200' : 'bg-ts-aquagreen-100')
                    : (
                      active ? 'bg-ts-gray-200' : '')
                  , 'cursor-default select-none relative py-2 pl-3 pr-9'
                )}
              >
                {({ selected, active }) => (
                  <div className='grid grid-cols-10'>
                    {selected ? (
                      <span
                        className='col-span-1 flex items-center text-ts-aquagreen-600'
                      >
                        <CheckIcon className="h-5 w-5" aria-hidden="true" />
                      </span>
                    ) : <span
                      className='col-span-1 flex items-center text-transparent'
                    >
                      <CheckIcon className="h-5 w-5" aria-hidden="true" />
                    </span>}

                    <img className="col-span-1 flex content-center mt-1 object-cover w-7 h-7 rounded-full" src={person.picture} alt="Profile image" />
                    <span className='col-span-6 flex items-center truncate ml-2'>
                      {person.name}
                    </span>
                    <span className='col-span-2 items-center flex-inline text-right text-gray-400'>
                      16 Task
                      <p className='text-xs'>on progress</p>
                    </span>

                  </div>
                )}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </Transition>
      </div>
    </Listbox>
  )
}